import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// Components
import Menu from './components/Menu';
import Header from './components/Header';
import Services from './components/services/Index';
import MyServicesList from './components/services/MyServicesList';
import ServicesForm from './components/services/ServicesForm';
import ServiceTypeList from './components/service-types/ServiceTypeList'
import AddressList from './components/addresses/AddressList'
import UserForm from './components/users/UserForm';
import UserTypesPermissionList from './components/resources/UserTypesPermissionList';
import Logout from './components/users/Logout'
import NotFound from './components/NotFound'
import PartnerServicesList from './components/services/PartnerServicesList'
import ResourceList from './components/resources/ResourceList';
import Dashboards from './components/dashboards/Index';
// Props
import { showModal } from './actions/response-modal';
import { backend } from './actions/backend';
// Cookies
import { getUserCookie, setUserCookie, getTokenCookie, setTokenCookie } from './store/cookies';
// Providers
import PermissionProvider from './PermissionProvider/PermissionProvider';
import PaymentMethodList from './components/payment-methods/PaymentMethodList';

class App extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.state = {
			permissions: []
		}
		this.getPermissions = this.getPermissions.bind(this);
	}

	async componentDidMount() {
		if (this.props.user === 'null' || !this.props.user)
			return this.history.push('/');
		await this.getPermissions()
	}

	componentDidUpdate() {
		if (this.props.user === 'null' || !this.props.user)
			return this.history.push('/');
	}

	async getPermissions() {
		try {
			if (this.state.permissions.length === 0) {
				const user = getUserCookie();
				const response = await this.props.backend({
					url: `${process.env.REACT_APP_API_URL}/user-types/${user.userType.id}/permissions`,
					method: 'get'
				})
				if ('error' in response) throw response.error;
				this.setState({
					permissions: response.payload.data.map(accessLevel => accessLevel.resource.name)
				})
			}
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (
			<PermissionProvider permissions={this.state.permissions} >
				<Router history={this.history}>
					<Header />
					<div className='wrapper'>
						<Menu />
						<div className='container-fluid p-4' id='content'>
							<Switch>
								<Route exact path='/services/new' component={ServicesForm}></Route>
								<Route exact path='/services/search' component={Services}></Route>
								<Route path='/services' component={MyServicesList}></Route>
								<Route exact path={['/service-types', '/service-types/:id']} component={ServiceTypeList}></Route>
								<Route exact path={['/addresses', '/addresses/:id']} component={AddressList}></Route>
								<Route path='/partner/services' component={PartnerServicesList}></Route>
								<Route path='/profile' component={UserForm}></Route>
								<Route path='/permissions' component={UserTypesPermissionList}></Route>
								<Route exact path={['/resources', '/resources/:id']} component={ResourceList}></Route>
								<Route exact path={['/payment-methods', '/payment-methods/:id']} component={PaymentMethodList}></Route>
								<Route exact path='/dashboards' component={Dashboards}></Route>
								<Route path='/logout' component={Logout}></Route>
								<Route path='*' component={NotFound}></Route>
							</Switch>
						</div>
					</div>
				</Router>
			</PermissionProvider>
		);
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ showModal, backend }, dispatch)
}

function mapStateToProps(state) {
	let user;
	let token;
	if (state.user) {
		setUserCookie(state.user.user);
		setTokenCookie(state.user.token);
		user = state.user.user;
		token = state.user.token;
	} else {
		user = getUserCookie();
		token = getTokenCookie();
	}
	return { user, token };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);