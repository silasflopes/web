import React from 'react';

const defaultBehaviour = {
	isAllowedTo: () => false
}

// Create the context
const PermissionContext = React.createContext(defaultBehaviour);

export default PermissionContext;