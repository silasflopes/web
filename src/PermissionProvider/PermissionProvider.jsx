import React from 'react';
import PermissionContext from "./PermissionContext";

const PermissionProvider = ({ permissions, children }) => {
	const isAllowedTo = (requiredPermissions) => {
		const match = requiredPermissions.filter(permission => permissions.includes(permission));
		return match.length === requiredPermissions.length;
	};

	return <PermissionContext.Provider value={{ isAllowedTo }}>{children}</PermissionContext.Provider>;
};

export default PermissionProvider;