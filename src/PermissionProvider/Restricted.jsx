import React from 'react';
import usePermission from "./usePermission";

const Restricted = ({ to, fallback, children }) => {
	// We "connect" to the provider thanks to the PermissionContext
	const allowed = usePermission(to);

	// If the user has that permission, render the children
	if (allowed) {
		return <>{children}</>;
	}

	// Otherwise, render the fallback
	return <>{fallback}</>;
};

export default Restricted;