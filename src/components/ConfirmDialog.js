import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';

class ConfirmDialog extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: ('show' in this.props && typeof this.props.show === 'boolean') ? this.props.show : true,
			bodyText: '',
			id: this.props.id
		}

		this.confirmAction = this.confirmAction.bind(this);
	}

	static getDerivedStateFromProps(props) {
		return {
			show: props.show,
			bodyText: props.bodyText,
			id: props.id
		}
	}

	confirmAction() {
		this.props.confirmAction(this.props.id);
	}

	render() {
		return (
			<Modal animation={false} show={this.state.show} onClick={this.props.closeModal} onHide={this.props.closeModal}>
				<Modal.Header closeButton>
					<Modal.Title>Tem certeza disso?</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<span>{this.state.bodyText}</span>
				</Modal.Body>
				<Modal.Footer>
					<a className='btn' onClick={this.props.closeModal}>Não</a>
					<button className='btn btn-primary' onClick={this.confirmAction}>Sim</button>
				</Modal.Footer>
			</Modal>
		)
	}
}

export default ConfirmDialog;