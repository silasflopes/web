import React, { Component } from 'react';
import { showModal } from '../../actions/response-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Select from 'react-select';
import { backend } from '../../actions/backend';
import { FaPlus } from 'react-icons/fa';
import Restricted from '../../PermissionProvider/Restricted';
import PaymentMethodForm from './PaymentMethodForm';
import PaymentMethodListItem from './PaymentMethodListItem';
import ConfirmDialog from '../ConfirmDialog';


class PaymentMethodList extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.state = {
			showFormDialog: this.props.match?.params.id ? true : false,
			showDeleteDialog: false,
			editItem: this.props.location?.state,
			payment_methods: [],
			paymentMethodList: [],
			selected: '',
			payments: '',
			deleteId: undefined,
			deleteText: ''
		}

		this.getList = this.getList.bind(this);
		this.onPaymentMethodsChange = this.onPaymentMethodsChange.bind(this);
		this.onPaymentMethodsChangeHidden = this.onPaymentMethodsChangeHidden.bind(this);
		this.showForm = this.showForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		this.onEditClick = this.onEditClick.bind(this);
		this.getPaymentMethod = this.getPaymentMethod.bind(this);
		this.closeDeleteDialog = this.closeDeleteDialog.bind(this);
		this.deletePaymentMethod = this.deletePaymentMethod.bind(this);
	}

	componentDidMount() {
		if (this.props.match?.params.id && !this.state.editItem)
			this.getPaymentMethod()

		this.getList();
	}

	async getPaymentMethod() {
		try {
			const response = await this.props.backend({
				method: 'get',
				url: `${process.env.REACT_APP_API_URL}/payment-methods/${this.props.match?.params.id}`
			})
			if ('error' in response) throw response.error;
			this.setState({
				editItem: response.payload.data
			})
		} catch (error) {
			console.log(error);
		}
	}

	async getList() {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/payment-methods`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			this.setState({
				payment_methods: response.payload.data.map(method => ({ value: method.id, label: method.name })),
				paymentMethodList: response.payload.data
			});
		} catch (error) {
			console.log(error);
		}
	}

	onPaymentMethodsChange(values) {
		this.setState({
			payments: JSON.stringify(values)
		})
		this.props.onChange(values.map(value => ({ payment_method_id: parseInt(value.value), name: value.label })))
	}
	onPaymentMethodsChangeHidden(values) {
		this.onPaymentMethodsChange(values)
	}

	static getDerivedStateFromProps(props, state) {
		if (props.selected !== state.selected) {
			return {
				selected: props.selected,
				payments: JSON.stringify(props.selected)
			}
		}
		return null;
	}

	showForm(event) {
		event.preventDefault();
		this.setState({
			showFormDialog: true
		});
	}

	closeForm(reload = false) {
		this.setState({
			showFormDialog: false
		});
		if (this.props.match?.params.id)
			this.history.push('/payment-methods')
		if (reload)
			this.getList();
	}

	onDeleteClick(item) {
		this.setState({
			showDeleteDialog: true,
			deleteId: item.id,
			deleteText: <span>Tem certeza que deseja excluir a forma de pagamento <strong>{item.name}</strong>?</span>
		});
	}

	onEditClick(item) {
		this.setState({
			showFormDialog: true,
			editItem: item
		})
		return this.history.push(`/payment-methods/${item.id}`, item)
	}

	async deletePaymentMethod(id) {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/payment-methods/${id}`,
				method: 'delete'
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Forma de pagamento excluída com sucesso');
			this.getList();
		} catch (error) {
			console.log(error);
		}
	}

	closeDeleteDialog() {
		this.setState({
			showDeleteDialog: false,
			deleteId: undefined
		})
	}

	render() {
		const list = (this.props.asInput) ?
			<div>
				<Select placeholder='Forma de pagamento...'
					isMulti={this.props.multiple}
					options={this.state.payment_methods}
					value={this.state.selected.map(selected => ({ value: selected.payment_method_id, label: selected.name }))}
					onChange={this.onPaymentMethodsChange} required />
				<input value={this.state.payments}
					tabIndex={-1}
					autoComplete="off"
					onChange={this.onPaymentMethodsChangeHidden}
					style={{ opacity: 0, height: 0, position: 'absolute' }} required />
			</div>
			:
			<ul className='list-group mt-2'>{this.state.paymentMethodList.map(paymentMethod => <PaymentMethodListItem onEditClick={this.onEditClick} onDeleteClick={this.onDeleteClick} asInput={this.props.asInput} key={paymentMethod.id} item={paymentMethod} />)}</ul>
		return (<div>
			<div className='text-end'>
				<Restricted to={['payment-method:read', 'payment-method:create']}><button className='btn' onClick={this.showForm}><FaPlus /></button></Restricted>
			</div>
			{this.state.paymentMethodList.length > 0 ? list : <p>Nenhuma forma de pagamento cadastrada ainda...</p>}
			<PaymentMethodForm closeModal={this.closeForm} item={this.state.editItem} show={this.state.showFormDialog} />
			<ConfirmDialog show={this.state.showDeleteDialog} id={this.state.deleteId} bodyText={this.state.deleteText}
				closeModal={this.closeDeleteDialog} confirmAction={this.deletePaymentMethod} />
		</div>);
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ showModal, backend }, dispatch)
}

export default connect(null, mapDispatchToProps)(PaymentMethodList);