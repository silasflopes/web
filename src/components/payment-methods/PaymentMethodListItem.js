import React, { Component } from 'react';
import Restricted from '../../PermissionProvider/Restricted';
import { FaEdit, FaTrash } from 'react-icons/fa';

class ServiceTypeListItem extends Component {
	constructor(props) {
		super(props);

		this.onEditClick = this.onEditClick.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
	}

	onEditClick() {
		this.props.onEditClick(this.props.item);
	}

	onDeleteClick() {
		this.props.onDeleteClick(this.props.item);
	}

	render() {
		return (
			<li value={this.props.item.id} className='list-group-item'>
				<div className='row'>
					<div className='col-6'>
						<span style={{ lineHeight: '38px' }}>{this.props.item.name}</span>
					</div>
					<div className='col-6 text-end'>
						<Restricted to={['admin', 'payment-method:read', 'payment-method:edit']}><button onClick={this.onEditClick} className='btn'><FaEdit /></button></Restricted>
						<Restricted to={['admin', 'payment-method:read', 'payment-method:delete']}><button onClick={this.onDeleteClick} className='btn'><FaTrash /></button></Restricted>
					</div>
				</div>
			</li>
		)
	}
}

export default ServiceTypeListItem;