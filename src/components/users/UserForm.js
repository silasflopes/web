import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import UserTypeList from '../user-types/UserTypeList';
import { backend } from '../../actions/backend';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import { getUserCookie } from '../../store/cookies';
import OfferedServiceTypes from '../offered-service-types/OfferedServiceTypes';
import PaymentMethodList from '../payment-methods/PaymentMethodList';
import Restricted from '../../PermissionProvider/Restricted';

class UserForm extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		const user = getUserCookie();
		this.state = {
			name: '',
			surname: '',
			password: '',
			confirm_password: '',
			email: '',
			user_type_id: '',
			birthdate: '',
			services: [],
			payments: [],
			user: user === 'null' ? undefined : user,
			load: false
		}

		this.onNameChange = this.onNameChange.bind(this);
		this.onSurnameChange = this.onSurnameChange.bind(this);
		this.onUserTypeChange = this.onUserTypeChange.bind(this);
		this.onEmailChange = this.onEmailChange.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);
		this.onConfirmPasswordChange = this.onConfirmPasswordChange.bind(this);
		this.onBirthdateChange = this.onBirthdateChange.bind(this);
		this.onServiceTypesChange = this.onServiceTypesChange.bind(this);
		this.onPaymentMethodsChange = this.onPaymentMethodsChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.getUser = this.getUser.bind(this);
		this.register = this.register.bind(this);
		this.edit = this.edit.bind(this);
	}

	componentDidMount() {
		this.getUser();
	}

	async getUser() {
		try {
			const requests = [{
				url: `${process.env.REACT_APP_API_URL}/users/${this.state.user.email}`,
				method: 'get'
			}];

			if (this.state.user.userType.id === 2) {
				requests.push({
					url: `${process.env.REACT_APP_API_URL}/users/${this.state.user.email}/service-types`,
					method: 'get'
				}, {
					url: `${process.env.REACT_APP_API_URL}/users/${this.state.user.email}/payment-methods`,
					method: 'get'
				});
			}
			const response = await this.props.backend(requests);
			if ('error' in response) throw response.error;
			const [userResponse, servicesResponse, paymentsResponse] = response.payload;
			this.setState({
				load: false,
				...userResponse.data,
				services: !servicesResponse ? [] : servicesResponse.data.map(serviceType => ({ base_value: serviceType.baseValue, service_type_id: serviceType.serviceType.id })),
				payments: !paymentsResponse ? [] : paymentsResponse.data.map(paymentMethod => ({ payment_method_id: paymentMethod.paymentMethod.id, name: paymentMethod.paymentMethod.name })),
				user_type_id: userResponse.data.userType.id
			});
		} catch (error) {
			this.setState({
				load: false
			});
		}
	}

	onNameChange(event) {
		this.setState({
			name: event.target.value
		})
	}
	onSurnameChange(event) {
		this.setState({
			surname: event.target.value
		})
	}
	onUserTypeChange(event) {
		this.setState({
			user_type_id: event.target.value
		})
	}
	onEmailChange(event) {
		this.setState({
			email: event.target.value
		})
	}
	onPasswordChange(event) {
		this.setState({
			password: event.target.value
		})
	}
	onConfirmPasswordChange(event) {
		this.setState({
			confirm_password: event.target.value
		});
	}
	onBirthdateChange(event) {
		this.setState({
			birthdate: event.target.value
		})
	}
	onServiceTypesChange(values) {
		this.setState({
			services: values.map(value => ({ service_type_id: parseInt(value.service_type_id), base_value: parseInt(value.base_value) }))
		});
	}
	onPaymentMethodsChange(values) {
		this.setState({
			payments: values
		});
	}

	async onSubmit(event) {
		event.preventDefault();
		if (this.state.user) this.edit();
		else this.register();
	}

	async register() {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users`,
				method: 'post',
				data: { ...this.state, user_type_id: parseInt(this.state.user_type_id) }
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Cadastro realizado com sucesso!');
			return this.history.push('/')
		} catch (error) {
			console.log(error);
		}
	}

	async edit() {
		const data = { ...this.state };
		if (this.state.password.length === 0 && this.state.confirm_password.length === 0) {
			delete data.password;
			delete data.confirm_password;
			delete data.user_type_id;
		}
		try {
			const user = getUserCookie();
			const requests = [{
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}`,
				method: 'patch',
				data
			}];
			if (this.state.user && this.state.user_type_id === 2) {
				requests.push({
					url: `${process.env.REACT_APP_API_URL}/users/${user.email}/service-types`,
					method: 'put',
					data: this.state.services.map(service => ({ base_value: parseInt(service.base_value), service_type_id: parseInt(service.service_type_id) }))
				}, {
					url: `${process.env.REACT_APP_API_URL}/users/${user.email}/payment-methods`,
					method: 'put',
					data: this.state.payments.map(payment => ({ payment_method_id: parseInt(payment.payment_method_id) }))
				})
			}
			const response = await this.props.backend(requests);

			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Editado com sucesso!');
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (<div className={(!this.state.user) ? 'container mt-4' : ''}>
			<form className='input-group' onSubmit={this.onSubmit}>
				<div className='row content-justify-center'>
					<div className='col-lg-4 mt-3'>
						<label className='form-label' htmlFor='name'>Nome: </label>
						<input onChange={this.onNameChange} value={this.state.name} id='name' className='form-control' type='text' placeholder='Nome' required />
					</div>
					<div className='col-lg-4 mt-3'>
						<label className='form-label' htmlFor='surname'>Sobrenome: </label>
						<input onChange={this.onSurnameChange} value={this.state.surname} id='surname' className='form-control' type='text' placeholder='Sobrenome' required />
					</div>
					<div className='col-lg-4 mt-3'>
						<label className='form-label' htmlFor='user-type'>Eu sou... </label>
						<UserTypeList onChange={this.onUserTypeChange} selected={this.state.user_type_id} id='user-type' placeholder='Eu sou...' asInput={true} required={true} disabled={this.state.user ? true : false} />
					</div>
					<div className='col-lg-6 mt-3'>
						<label className='form-label' htmlFor='birthdate'>Data de nascimento: </label>
						<input onChange={this.onBirthdateChange} value={this.state.birthdate} id='birthdate' className='form-control' type='date' placeholder='Data de nascimento' required />
					</div>
					<div className='col-lg-6 mt-3'>
						<label className='form-label' htmlFor='email'>E-mail: </label>
						<input onChange={this.onEmailChange} value={this.state.email} id='email' className='form-control' type='email' placeholder='E-mail' required disabled={this.state.user ? true : false} />
					</div>
					<div className='col-lg-6 mt-3'>
						<label className='form-label' htmlFor='password'>Senha: </label>
						<input onChange={this.onPasswordChange} value={this.state.password} id='password' className='form-control' type='password' placeholder='Senha' required={this.state.user ? false : true} />
					</div>
					<div className='col-lg-6 mt-3'>
						<label className='form-label' htmlFor='confirm-password'>Confirmar senha: </label>
						<input onChange={this.onConfirmPasswordChange} value={this.state.confirm_password} id='confirm-password' className='form-control' type='password' placeholder='Confirmar senha' required={this.state.user ? false : true} />
					</div>
					<Restricted to={['user:read', 'service-type:read', 'offered-service:create', 'offered-service:delete']}>
						<div className='col-lg-6 mt-3'>
							<label className='form-label' htmlFor='service-types-list'>Serviços que ofereço:</label>
							<OfferedServiceTypes asInput={true} multiple={true} selected={this.state.services} onChange={this.onServiceTypesChange} />
						</div>
					</Restricted>
					<Restricted to={['user:read', 'payment-method:read', 'accepted-payment:create', 'accepted-payment:delete']}>
						<div className='col-lg-6 mt-3'>
							<label className='form-label'>Formas de pagamento:</label>
							<PaymentMethodList onChange={this.onPaymentMethodsChange} selected={this.state.payments} id='payment-method' placeholder='Eu sou...' multiple={true} asInput={true} required={true} />
						</div>
					</Restricted>
					<div className='col-12 text-end mt-3'>
						{this.state.user ? '' : <Link to='/' className='btn'>Voltar</Link>}
						<button className='btn btn-primary'>{this.state.user ? 'Salvar' : 'Registrar'}</button>
					</div>
				</div>
			</form>
		</div>);
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ showModal, backend }, dispatch)
}

export default connect(null, mapDispatchToProps)(withRouter(UserForm));