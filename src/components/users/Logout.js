import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '../../actions'

class Logout extends Component {
	constructor(props) {
		super(props);
	}

	componentWillUnmount() {
		this.props.logout();
	}

	render() {
		return (
			<Redirect to='/'></Redirect>
		)
	}
}
function mapDispatchToProps(dispatch) {
	return bindActionCreators({ logout }, dispatch)
}

export default connect(null, mapDispatchToProps)(Logout);