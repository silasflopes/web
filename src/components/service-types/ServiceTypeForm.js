import React, { Component } from 'react';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import { Modal } from 'react-bootstrap';

class ServiceTypeForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			item: this.props.item,
			id: this.props.item ? this.props.item.id : '',
			name: this.props.item ? this.props.item.name : '',
			show: (this.props.show) ? true : false
		}

		this.onNameChange = this.onNameChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.closeForm = this.closeForm.bind(this);
	}

	componentDidMount() {
		if (this.state.show || this.state.item) {
			this.setState({
				show: true
			})
		}
	}

	onNameChange(event) {
		this.setState({
			name: event.target.value
		});
	}

	static getDerivedStateFromProps(props, state) {
		if ((props.item && !state.item) || (props.item && state.item && props.item.id !== state.item.id))
			return {
				name: props.item.name,
				item: props.item,
				show: props.show
			}
		return {
			show: props.show
		}
	}

	async onSubmit(event) {
		event.preventDefault();
		try {
			const request = (this.state.item) ? {
				url: `${process.env.REACT_APP_API_URL}/service-types/${this.state.item.id}`,
				method: 'patch',
				data: { name: this.state.name }
			} : {
				url: `${process.env.REACT_APP_API_URL}/service-types`,
				method: 'post',
				data: { name: this.state.name }
			}
			const response = await this.props.backend(request);
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Tipo de serviço salvo com sucesso');
			this.props.closeModal(true);
		} catch (error) {
			console.log(error);
		}
	}

	closeForm() {
		this.props.closeModal(false);
	}

	render() {
		return (
			<Modal animation={false} show={this.state.show} onHide={this.closeForm} centered>
				<form id='service-type-form' onSubmit={this.onSubmit}>
					<Modal.Header closeButton>
						<Modal.Title>Adicionar tipo de serviço</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div>
							<label className='form-label' htmlFor='service-name'>Nome do serviço: </label>
							<input id='service-name' className='form-control' placeholder='Nome do serviço' type='text' value={this.state.name} onChange={this.onNameChange} required />
						</div>
					</Modal.Body>
					<Modal.Footer>
						<a className='btn' onClick={this.closeForm}>Cancelar</a>
						<button className='btn btn-primary'>Salvar</button>
					</Modal.Footer>
				</form>
			</Modal>
		)
	}
}

export default connect(null, { showModal, backend })(ServiceTypeForm);