import React, { Component } from 'react';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import ServiceTypeListItem from './ServiceTypeListItem';
import { FaPlus } from 'react-icons/fa';
import ServiceTypeForm from './ServiceTypeForm';
import Select from 'react-select';
import Restricted from '../../PermissionProvider/Restricted';
import ConfirmDialog from '../ConfirmDialog';

class ServiceTypeList extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.state = {
			showFormDialog: this.props.match?.params.id ? true : false,
			showDeleteDialog: false,
			editItem: this.props.location?.state,
			serviceTypeList: [],
			serviceTypeOptions: [],
			value: '',
			selectedIndex: 0,
			deleteId: undefined,
			deleteText: ''
		}

		this.getList = this.getList.bind(this);
		this.showForm = this.showForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.handleOnChange = this.handleOnChange.bind(this);
		this.handleOnChangeHidden = this.handleOnChangeHidden.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		this.onEditClick = this.onEditClick.bind(this);
		this.getServiceType = this.getServiceType.bind(this);
		this.closeDeleteDialog = this.closeDeleteDialog.bind(this);
		this.deleteServiceType = this.deleteServiceType.bind(this);
	}

	componentDidMount() {
		if (this.props.match?.params.id && !this.state.editItem)
			this.getServiceType()

		this.getList()
	}

	async getServiceType() {
		try {
			const response = await this.props.backend({
				method: 'get',
				url: `${process.env.REACT_APP_API_URL}/service-types/${this.props.match?.params.id}`
			})
			if ('error' in response) throw response.error;
			this.setState({
				editItem: response.payload.data
			})
		} catch (error) {
			console.log(error);
		}
	}

	async getList() {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/service-types`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			this.setState({
				serviceTypeOptions: response.payload.data.map(item => ({ value: item.id, label: item.name })),
				serviceTypeList: response.payload.data
			});
		} catch (error) {
			console.log(error);
		}
	}

	showForm(event) {
		event.preventDefault();
		this.setState({
			showFormDialog: true,
			editItem: undefined
		});
	}

	closeForm(reload = false) {
		this.setState({
			showFormDialog: false,
			editItem: undefined
		});
		if (this.state.editItem)
			this.history.push('/service-types');
		if (reload)
			this.getList();
	}

	handleOnChange(serviceType) {
		this.setState({
			value: serviceType.value
		})
		this.props.onChange(serviceType.value);
	}

	handleOnChangeHidden(event) {
		this.handleOnChange({ value: event.target.value });
	}

	static getDerivedStateFromProps(props, state) {
		if (props.value !== state.value) {
			return {
				value: props.value
			}
		}
		return null;
	}

	onDeleteClick(item) {
		this.setState({
			showDeleteDialog: true,
			deleteId: item.id,
			deleteText: <span>Tem certeza que deseja excluir o tipo de serviço <strong>{item.name}</strong>?</span>
		});
	}

	onEditClick(item) {
		this.setState({
			showFormDialog: true,
			editItem: item
		})
		return this.history.push(`/service-types/${item.id}`, item)
	}

	async deleteServiceType(id) {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/service-types/${id}`,
				method: 'delete'
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Tipo de serviço excluído com sucesso');
			this.getList();
		} catch (error) {
			console.log(error);
		}
	}

	closeDeleteDialog() {
		this.setState({
			showDeleteDialog: false,
			deleteId: undefined
		})
	}

	render() {
		const list = (this.props.asInput) ?
			<div>
				<Select placeholder='Tipo de serviço...'
					options={this.state.serviceTypeOptions}
					value={this.state.value !== '' ? this.state.serviceTypeOptions.find(serviceType => serviceType.value === this.state.value) : null}
					onChange={this.handleOnChange} required />
				<input value={this.state.value}
					tabIndex={-1}
					autoComplete="off"
					onChange={this.handleOnChangeHidden}
					style={{ opacity: 0, height: 0, position: 'absolute' }} required />
			</div>
			:
			<ul className='list-group mt-2'>{this.state.serviceTypeList.map(serviceType => <ServiceTypeListItem onEditClick={this.onEditClick} onDeleteClick={this.onDeleteClick} asInput={this.props.asInput} key={serviceType.code} item={serviceType} />)}</ul>
		return (<div>
			<div className='text-end'>
				<Restricted to={['service-type:read', 'service-type:create']}><button className={'hideAddButton' in this.props && this.props.hideAddButton ? 'd-none' : 'btn'} onClick={this.showForm}><FaPlus /></button></Restricted>
			</div>
			{this.state.serviceTypeList.length > 0 ? list : <p>Nenhum serviço cadastrado ainda...</p>}
			<ServiceTypeForm closeModal={this.closeForm} item={this.state.editItem} show={this.state.showFormDialog} />
			<ConfirmDialog show={this.state.showDeleteDialog} id={this.state.deleteId} bodyText={this.state.deleteText}
				closeModal={this.closeDeleteDialog} confirmAction={this.deleteServiceType} />
		</div>);
	}
}

export default connect(null, { showModal, backend })(ServiceTypeList);