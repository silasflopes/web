import React, { Component } from 'react'
import { Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import { backend } from '../../actions/backend';
import { showModal } from '../../actions/response-modal';
import { getUserCookie } from '../../store/cookies';
import ReactStars from 'react-rating-stars-component';

class ReviewView extends Component {
	constructor(props) {
		super(props)

		this.state = {
			show: this.props.show,
			review: this.props.review,
			service: this.props.service,
			rate: 0,
			comment: ''
		}

		this.user = getUserCookie();
		this.onCommentChange = this.onCommentChange.bind(this);
		this.onRateChange = this.onRateChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	onCommentChange(event) {
		this.setState({
			comment: event.target.value
		});
	}

	onRateChange(rate) {
		this.setState({
			rate
		})
	}

	async onSubmit(event) {
		event.preventDefault()
		try {
			const data = {
				rate: this.state.rate
			}
			if (this.state.comment.trim().length > 0)
				data.comment = this.state.comment.trim();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${this.user.email}/services/${this.state.service.id}/reviews`,
				data,
				method: 'post'
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Avaliação salva com sucesso')
			this.props.reloadList(this.state.service)
		} catch (error) {
			console.log(error);
		}
	}

	static getDerivedStateFromProps(props, state) {
		if (props.review && state.review && props.review.id !== state.review.id) {
			return {
				review: state.review
			}
		} else if (props.show !== state.show) {
			return {
				show: props.show
			}
		} else return null;
	}

	render() {
		const reviewForm = <div className='form-group'>
			<div className='row'>
				<div className='col-auto'>
					<label className='form-label' style={{ lineHeight: '32px' }} htmlFor='rate'>Nota:</label>
				</div>
				<div className='col-auto'>
					<ReactStars count={5} isHalf={false} size={20} onChange={this.onRateChange} />
				</div>
			</div>
			<label className='form-label' htmlFor='comment'>Comentário:</label>
			<textarea onChange={this.onCommentChange} id='comment' className='form-control' rows='5' style={{ resize: 'none' }}></textarea>
		</div>

		const review = this.state.review ? <div>
			<div><ReactStars count={5} size={20} value={this.state.review.rate} edit={false} /></div>
			<div>{this.state.review.comment ? `${this.state.review.user.name}: ${this.state.review.comment}` : ''}</div>
		</div> : <span>Nenhuma avaliação foi feita ainda.</span>

		return (
			<Modal animation={false} show={this.state.show} onHide={this.props.closeModal} centered>
				<form id='review-form' onSubmit={this.onSubmit}>
					<Modal.Header closeButton>
						<Modal.Title>Avaliação</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className='my-2'>
							{review}
						</div>
						<div className='my-2'>
							{!this.state.review && this.user.email === this.state.service.user.email ? reviewForm : ''}
						</div>
					</Modal.Body>
					<Modal.Footer>
						<a className='btn' onClick={this.props.closeModal}>Voltar</a>
						{this.state.review || this.user.email !== this.state.service.user.email ? '' : <button className='btn btn-primary'>Salvar</button>}
					</Modal.Footer>
				</form>
			</Modal>
		)
	}
}

export default connect(null, { backend, showModal })(ReviewView);