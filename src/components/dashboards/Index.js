import React, { Component } from 'react';
import Restricted from '../../PermissionProvider/Restricted';
import MonthlyIncome from './MonthlyIncome';
import YearlyIncome from './YearlyIncome';
import ServicesPerMonth from './ServicesPerMonth';
import DailyRegistration from './DailyRegistration';

class Dashboards extends Component {
	constructor(props){
		super(props)
	}

	render(){
		return(<div className='row justify-content-center'>
			<Restricted to={['dashboard:monthly-income']}><MonthlyIncome /></Restricted>
			<Restricted to={['dashboard:yearly-income']}><YearlyIncome /></Restricted>
			<Restricted to={['dashboard:services-per-month']}><ServicesPerMonth /></Restricted>
			<Restricted to={['dashboard:daily-registration']}><DailyRegistration /></Restricted>
		</div>)
	}
}
export function getRandomInt(max) {
	return Math.floor(Math.random() * max);
}

export default Dashboards;