import React, { Component } from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { getRandomInt } from './Index';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);
class MonthlyIncome extends Component {
	constructor(props){
		super(props)

		this.state = {
			data: []
		}

		this.getData = this.getData.bind(this);
		this.months = [
			'Jan',
			'Fev',
			'Mar',
			'Abr',
			'Mai',
			'Jun',
			'Jul',
			'Ago',
			'Set',
			'Out',
			'Nov',
			'Dez'
		]
	}

	componentDidMount(){
		this.getData()
	}
	async getData(){
		try{
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/dashboards/services-per-month`,
				method: 'get'
			})
			if ('error' in response) throw response.error;
			this.setState({
				data: response.payload.data
			})
		}catch(error){
			console.log(error);
		}
	}

	render(){
		if(this.state.data.length === 0)
			return(<div className='col-lg-8'>Serviços abertos por mês: sem dados</div>)

		const perServiceType = {}
		
		this.state.data.forEach(monthData => {
			if(!(monthData.service_name in perServiceType))
				perServiceType[monthData.service_name] = {data: this.months.map(() => 0), label: monthData.service_name, backgroundColor: `rgb(${getRandomInt(255)}, ${getRandomInt(255)}, ${getRandomInt(255)})`};
			
			perServiceType[monthData.service_name].data[monthData.month - 1] = monthData.quantity;
		})

		const data = {
			labels: this.months,
			datasets: Object.values(perServiceType) 
		}
		const options = {
			plugins: {
				title: {
					display: true,
					text: 'Serviços abertos por mês',
				}
			},
			responsive: true,
			scales: {
				x: {
					stacked: true,
				},
				y: {
					stacked: true
				},
			},
		};
		return(
			<div className='col-lg-8'>
				<Bar options={options} data={data} />
			</div>
		)
	}
}
export default connect(null, { backend })(MonthlyIncome);