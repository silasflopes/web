import React, { Component } from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { getRandomInt } from './Index';
import moment from 'moment-timezone';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);
class DailyRegistration extends Component {
	constructor(props){
		super(props)

		this.state = {
			data: []
		}

		this.months = [
			'Janeiro',
			'Fevereiro',
			'Março',
			'Abril',
			'Maio',
			'Junho',
			'Julho',
			'Agosto',
			'Setembro',
			'Outubro',
			'Novembro',
			'Dezembro'
		]

		this.getData = this.getData.bind(this);
	}

	componentDidMount(){
		this.getData()
	}
	async getData(){
		try{
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/dashboards/daily-registration`,
				method: 'get'
			})
			if ('error' in response) throw response.error;
			this.setState({
				data: response.payload.data
			})
		}catch(error){
			console.log(error);
		}
	}

	render(){
		if(this.state.data.length === 0)
			return(<div className='col-lg-auto'>Registros diários: sem dados</div>)

		const perUserType = {}
		const current = moment();
		const month = current.month();
		const lastDayOfMonth = current.endOf('month').date();
		const monthDays = [];
		for(let i = 1; i <= lastDayOfMonth; i++){
			monthDays.push(`${(i).toString().padStart(2, '0')}/${ (month + 1).toString().padStart(2, '0') }`);
		}

		this.state.data.forEach(dailyData => {
			if(!(dailyData.user_type in perUserType))
				perUserType[dailyData.user_type] = {
					data: monthDays.map(() => 0), 
					label: dailyData.user_type, 
					backgroundColor: `rgb(${getRandomInt(255)}, ${getRandomInt(255)}, ${getRandomInt(255)})`
				};
			
			perUserType[dailyData.user_type].data[dailyData.day - 1] = dailyData.quantity;
		})

		const data = {
			labels: monthDays,
			datasets: Object.values(perUserType) 
		}
		const options = {
			plugins: {
				title: {
					display: true,
					text: `Registros diários (${this.months[month]})`,
				}
			},
			responsive: true,
			scales: {
				x: {
					stacked: true
				},
				y: {
					stacked: true
				},
			},
		};
		return(
			<div className='col-lg-8'>
				<Bar options={options} data={data} />
			</div>
		)
	}
}
export default connect(null, { backend })(DailyRegistration);