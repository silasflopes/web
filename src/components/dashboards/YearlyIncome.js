import React, { Component } from 'react';
import {
  Chart as ChartJS,
  ArcElement,
  Tooltip,
  Legend,
} from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { getRandomInt } from './Index';

ChartJS.register(
	ArcElement,
  Tooltip,
  Legend
);
class YearlyIncome extends Component {
	constructor(props){
		super(props)

		this.state = {
			data: []
		}

		this.getData = this.getData.bind(this);
	}

	componentDidMount(){
		this.getData()
	}
	async getData(){
		try{
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/dashboards/yearly-income`,
				method: 'get'
			})
			if ('error' in response) throw response.error;
			this.setState({
				data: response.payload.data
			})
		}catch(error){
			console.log(error);
		}
	}

	render(){
		if(this.state.data.length === 0)
			return(<div className='col-lg-8'>Receita anual: sem dados</div>)

		// const perServiceType = {}
		
		// this.state.data.forEach(monthData => {
		// 	if(!(monthData.service_name in perServiceType))
		// 		perServiceType[monthData.service_name] = {data: this.months.map(() => 0), label: monthData.service_name, backgroundColor: };
			
		// 	perServiceType[monthData.service_name].data[monthData.month - 1] = monthData.income;
		// })

		const data = {
			labels: this.state.data.map(serviceType => serviceType.service_name),
			datasets: [{
				label: 'aaaa',
				data: this.state.data.map(serviceType => serviceType.income),
				backgroundColor: this.state.data.map(() => `rgb(${getRandomInt(255)}, ${getRandomInt(255)}, ${getRandomInt(255)})`)
			}]
		}
		const options = {
			plugins: {
				title: {
					display: true,
					text: 'Receita por serviço',
				}
			},
			responsive: true
		};
		return(
			<div className='col-lg-auto'>
				<Pie options={options} data={data} />
			</div>
		)
	}
}
export default connect(null, { backend })(YearlyIncome);