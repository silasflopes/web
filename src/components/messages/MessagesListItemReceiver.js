import React, { Component } from 'react';
import moment from 'moment-timezone';

class MessagesListItemReceiver extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (<div className='row m-2 talk-bubble talk-left'>
			<div className='receiver-bubble col-auto rounded py-1'>
				<div>{this.props.message.message}</div>
				<div className='text-info text-end'><small>{moment(this.props.message.createdAt).format('DD/MM/YYYY HH:mm')}</small></div>
			</div>
		</div>);
	}
}

export default MessagesListItemReceiver;