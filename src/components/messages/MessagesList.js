import React, { Component } from 'react';
import { backend } from '../../actions/backend';
import { showModal } from '../../actions/response-modal';
import { connect } from 'react-redux';
import { getUserCookie } from '../../store/cookies';
import { FaPaperPlane, FaEllipsisV } from 'react-icons/fa';
import moment from 'moment-timezone';
// Components
import MessagesListItem from './MessagesListItem';
import Restricted from '../../PermissionProvider/Restricted';
import ServiceView from '../services/ServiceView';
import ConfirmDialog from '../ConfirmDialog';
import ReviewView from '../reviews/ReviewView';

class MessagesList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			load: false,
			service: this.props.service,
			messagesList: [],
			message: '',
			showService: false,
			showFinishDialog: false,
			showCancelDialog: false,
			showReviewDialog: false
		}
		this.messagesListHolder = React.createRef();

		this.getList = this.getList.bind(this);
		this.onMessageChange = this.onMessageChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.onKeyUp = this.onKeyUp.bind(this);
		this.openServiceDialog = this.openServiceDialog.bind(this);
		this.closeService = this.closeService.bind(this);
		this.closeFinishDialog = this.closeFinishDialog.bind(this);
		this.closeCancelDialog = this.closeCancelDialog.bind(this);
		this.closeReviewDialog = this.closeReviewDialog.bind(this);
		this.confirmFinish = this.confirmFinish.bind(this);
		this.confirmCancel = this.confirmCancel.bind(this);
		this.openFinishDialog = this.openFinishDialog.bind(this);
		this.openCancelDialog = this.openCancelDialog.bind(this);
		this.openReviewDialog = this.openReviewDialog.bind(this);
	}

	componentDidMount() {
		this.setState({
			load: true
		});
	}

	async componentDidUpdate() {
		if (!this.state.service) {
			const service = await this.props.getService(this.props.location.pathname.match(/\d+/)[0])
			this.props.setActive(service);
		} else if (this.state.load)
			this.getList();
	}

	async getList(showLoading = true) {
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				method: 'get',
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/services/${this.state.service.id}/messages`
			}, showLoading);
			if ('error' in response.payload) throw response.payload.error;
			this.setState({
				load: false,
				messagesList: response.payload.data
			});
			if (this.messagesListHolder.current)
				this.messagesListHolder.current.scrollTop = this.messagesListHolder.current.scrollHeight
		} catch (error) {
			this.setState({
				load: false
			});
			console.log(error);
		}
	}

	static getDerivedStateFromProps(props, state) {
		if (props.service && props.service.id !== state.service.id)
			return { service: props.location.state, load: true, message: '' };
		return null;
	}

	onMessageChange(event) {
		this.setState({
			message: event.target.value
		});
	}

	onKeyUp(event) {
		if (event.keyCode === 13) {
			this.setState({
				message: event.target.value.trim()
			});
			this.onSubmit(event);
		}

	}

	async onSubmit(event) {
		event.preventDefault();
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/services/${this.state.service.id}/messages`,
				method: 'post',
				data: {
					message: this.state.message
				}
			}, false);
			if ('error' in response) throw response.error;
			this.getList(false)
			this.setState({
				message: ''
			})
		} catch (error) {
			console.log(error);
		}
	}

	openServiceDialog() {
		this.setState({
			showService: true
		});
	}

	closeService() {
		this.setState({
			showService: false
		})
	}

	closeFinishDialog() {
		this.setState({
			showFinishDialog: false
		})
	}

	closeCancelDialog() {
		this.setState({
			showCancelDialog: false
		})
	}

	closeReviewDialog() {
		this.setState({
			showReviewDialog: false
		})
	}

	async confirmFinish() {
		await this.editService({
			status_id: 3
		}, 'Serviço encerrado com sucesso')
	}

	async confirmCancel() {
		await this.editService({
			status_id: 2
		}, 'Serviço cancelado com sucesso')
	}

	openFinishDialog() {
		this.setState({
			showFinishDialog: true
		})
	}

	openCancelDialog() {
		this.setState({
			showCancelDialog: true
		})
	}

	openReviewDialog() {
		this.setState({
			showReviewDialog: true
		})
	}

	getUrl() {
		return this.props.partnerView ? `${process.env.REACT_APP_API_URL}/partners/services/${this.state.service.id}`
			: `${process.env.REACT_APP_API_URL}/users/${this.state.service.user.email}/services/${this.state.service.id}`;
	}

	async editService(data, successMessage) {
		try {
			const url = this.getUrl();
			const request = {
				url,
				method: 'patch',
				data
			}
			const response = await this.props.backend(request);
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', successMessage);
			this.props.reloadList(response.payload.data);
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (<div className='h-100 overflow-hidden d-flex flex-column'>
			<div className='navbar service-details'>
				<span className='ps-4'>{
					this.state.service ?
						`${this.props.partnerView ? this.state.service.user.name : this.state.service.professional.name} - ${this.state.service.serviceType.name} - ${this.state.service.status.name}${[2, 3].includes(this.state.service.status.id) ? ` em: ${moment(this.state.service.finishedAt).format('DD/MM/YYYY HH:mm')}` : ''}` : 'Carregando'}</span>
				<div className='btn-group text-end me-2 dropstart'>
					<button className='btn' type='button' data-bs-toggle='dropdown' aria-expanded='false'><FaEllipsisV /></button>
					<ul className='dropdown-menu'>
						<Restricted to={['user:read', 'service:read']}>
							<li><a className='cursor-pointer dropdown-item' onClick={this.openServiceDialog}>Ver serviço</a></li>
							{this.state.service ? <ServiceView partnerView={this.props.partnerView} reloadList={this.props.reloadList} service={this.state.service} show={this.state.showService} closeModal={this.closeService} /> : ''}
						</Restricted>
						<Restricted to={['user:read', 'service:read', 'review:read']}>
							<li><a className='cursor-pointer dropdown-item' onClick={this.openReviewDialog}>Ver avaliação</a></li>
							{this.state.service ? <ReviewView reloadList={this.props.reloadList} service={this.state.service} review={this.state.service.review} show={this.state.showReviewDialog} closeModal={this.closeReviewDialog} /> : ''}
						</Restricted>
						<Restricted to={['user:read', 'service:read', 'service:edit']}>
							<ConfirmDialog show={this.state.showFinishDialog} id={this.state.service ? this.state.service.id : null} bodyText={'Tem certeza que deseja encerrar este serviço?'} closeModal={this.closeFinishDialog} confirmAction={this.confirmFinish} />
							<li><a className='cursor-pointer dropdown-item' onClick={this.openFinishDialog}>Encerrar serviço</a></li>
						</Restricted>
						<Restricted to={['user:read', 'service:read', 'service:edit']}>
							<div className="dropdown-divider"></div>
							<ConfirmDialog show={this.state.showCancelDialog} id={this.state.service ? this.state.service.id : null} bodyText={'Tem certeza que deseja cancelar este serviço?'} closeModal={this.closeCancelDialog} confirmAction={this.confirmCancel} />
							<li><a className='cursor-pointer dropdown-item bg-danger text-white' onClick={this.openCancelDialog}>Cancelar serviço</a></li>
						</Restricted>
					</ul>
				</div>
			</div>
			<div className='overflow-y-auto overflow-x-hidden fill-height' ref={this.messagesListHolder}>
				{this.state.messagesList.map(message => <MessagesListItem key={message.id} item={message} />)}
			</div>
			<div className='w-100 input-group p-2 active'>
				<textarea value={this.state.message} onKeyUp={this.onKeyUp} onChange={this.onMessageChange} style={{ resize: 'none' }} rows='1' id='send-message' className='form-control rounded-pill' type='text' placeholder='Envie uma mensagem para iniciar uma conversa...' />
				<a className='cursor-pointer input-group-text' onClick={this.onSubmit}><FaPaperPlane /></a>
			</div>
		</div>)
	}
}

export default connect(null, { backend, showModal })(MessagesList);