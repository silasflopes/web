import React, { Component } from 'react';
import moment from 'moment-timezone';

class MessagesListItemSender extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (<div className='row justify-content-end m-2 talk-bubble talk-right'>
			<div className='border border-white bg-white col-auto rounded py-1'>
				<div>{this.props.message.message}</div>
				<div className='text-info text-end'><small>{moment(this.props.message.createdAt).format('DD/MM/YYYY HH:mm')}</small></div>
			</div>
		</div>);
	}
}

export default MessagesListItemSender;