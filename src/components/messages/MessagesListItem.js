import React, { Component } from 'react';
import { getUserCookie } from '../../store/cookies';
import MessagesListItemReceiver from './MessagesListItemReceiver';
import MessagesListItemSender from './MessagesListItemSender';

class MessageListItem extends Component {
	constructor(props) {
		super(props);

		const user = getUserCookie();
		this.state = {
			user
		}
	}

	render() {
		return (this.props.item.from.email === this.state.user.email) ?
			<MessagesListItemSender message={this.props.item} /> :
			<MessagesListItemReceiver message={this.props.item} />;
	}
}

export default MessageListItem;