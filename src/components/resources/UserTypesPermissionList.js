import React, { Component } from 'react';
import { connect } from 'react-redux';
import PermissionListItem from './PermissionList';
import PermissionsForm from './PermissionsForm';
import { showModal } from '../../actions/response-modal';
import { backend } from '../../actions/backend';

class UserTypesPermissionList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			showForm: false,
			userTypesPermissionList: [],
			resourcesList: [],
			availableResources: [],
			userTypeToAdd: 0
		}

		this.onAddClick = this.onAddClick.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.reloadList = this.reloadList.bind(this);
	}

	componentDidMount() {
		this.getList();
	}

	async getList() {
		try {
			const response = (await this.props.backend([{
				url: `${process.env.REACT_APP_API_URL}/user-types/permissions`,
				method: 'get'
			}, {
				url: `${process.env.REACT_APP_API_URL}/resources`,
				method: 'get'
			}]));
			if ('error' in response) throw response.error;
			const [userTypesResponse, resourcesResponse] = response.payload;
			this.setState({
				userTypesPermissionList: userTypesResponse.data,
				resourcesList: resourcesResponse.data
			});
		} catch (error) {
			console.log(error);
		}
	}

	onAddClick(userType) {
		const userResourcesIds = userType.permissions.map(permission => permission.resource.id);
		this.setState({
			showForm: true,
			availableResources: this.state.resourcesList.filter(resource => !userResourcesIds.includes(resource.id)),
			userTypeToAdd: userType.id
		})
	}

	closeForm(reload = false) {
		this.setState({
			showForm: false
		});
		if (reload)
			this.reloadList()
	}

	reloadList() {
		this.getList();
	}

	render() {
		return (<div>
			{this.state.userTypesPermissionList.map(userTypePermission => <PermissionListItem onAddClick={this.onAddClick} reloadList={this.reloadList} key={userTypePermission.id} userType={userTypePermission} />)}
			<PermissionsForm show={this.state.showForm} close={this.closeForm} resources={this.state.availableResources} userTypeId={this.state.userTypeToAdd} />
		</div>)
	}
}

export default connect(null, { showModal, backend })(UserTypesPermissionList);