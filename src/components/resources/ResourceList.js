import React, { Component } from 'react';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import ResourceListItem from './ResourceListItem';
import { FaPlus } from 'react-icons/fa';
import ResourceForm from './ResourceForm';
import Restricted from '../../PermissionProvider/Restricted';
import ConfirmDialog from '../ConfirmDialog';

class ResourceList extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.state = {
			showFormDialog: this.props.match?.params.id ? true : false,
			showDeleteDialog: false,
			editItem: this.props.location.state,
			resourceList: [],
			resourceOptions: [],
			load: false,
			deleteId: undefined,
			deleteText: ''
		}

		this.getList = this.getList.bind(this);
		this.showForm = this.showForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		this.onEditClick = this.onEditClick.bind(this);
		this.getResource = this.getResource.bind(this);
		this.closeDeleteDialog = this.closeDeleteDialog.bind(this);
		this.deleteResource = this.deleteResource.bind(this);
	}

	componentDidMount() {
		if (this.props.match?.params.id && !this.state.editItem)
			this.getResource()

		this.getList();
	}

	async getResource() {
		try {
			const response = await this.props.backend({
				method: 'get',
				url: `${process.env.REACT_APP_API_URL}/resources/${this.props.match?.params.id}`
			})
			if ('error' in response) throw response.error;
			this.setState({
				editItem: response.payload.data
			})
		} catch (error) {
			console.log(error);
		}
	}

	async getList() {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/resources`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			this.setState(({
				resourceOptions: response.payload.data.map(item => ({ value: item.id, label: item.name })),
				resourceList: response.payload.data
			}));
		} catch (error) {
			console.log(error);
		}
	}

	async deleteResource(id) {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/resources/${id}`,
				method: 'delete'
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Recurso do sistema excluído com sucesso');
			this.getList();
		} catch (error) {
			console.log(error);
		}
	}

	showForm(event) {
		event.preventDefault();
		this.setState({
			showFormDialog: true,
			editItem: undefined
		});
	}

	closeForm(reload = false) {
		this.setState({
			showFormDialog: false,
			editItem: undefined
		});
		if (this.state.editItem)
			this.history.push('/resources');
		if (reload)
			this.getList();
	}

	onDeleteClick(item) {
		this.setState({
			showDeleteDialog: true,
			deleteId: item.id,
			deleteText: <span>Tem certeza que deseja excluir o recurso <strong>{item.name}</strong>?</span>
		});
	}

	onEditClick(item) {
		this.setState({
			showFormDialog: true,
			editItem: item
		})
		return this.history.push(`/resources/${item.id}`, item)
	}
	closeDeleteDialog() {
		this.setState({
			showDeleteDialog: false,
			deleteId: undefined
		})
	}

	render() {
		return (<div>
			<div className='text-end'>
				<Restricted to={['resource:read', 'resource:create']}><button className='btn' onClick={this.showForm}><FaPlus /></button></Restricted>
			</div>
			{this.state.resourceList.length > 0
				? <ul className='list-group mt-2'>{this.state.resourceList.map(resource => <ResourceListItem onEditClick={this.onEditClick} onDeleteClick={this.onDeleteClick} key={resource.id} item={resource} />)}</ul>
				: <p>Nenhum recurso cadastrado ainda...</p>}
			<ResourceForm closeModal={this.closeForm} item={this.state.editItem} show={this.state.showFormDialog} />
			<ConfirmDialog show={this.state.showDeleteDialog} id={this.state.deleteId} bodyText={this.state.deleteText}
				closeModal={this.closeDeleteDialog} confirmAction={this.deleteResource} />
		</div>);
	}
}

export default connect(null, { showModal, backend })(ResourceList);