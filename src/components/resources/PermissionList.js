import React, { Component } from 'react';
import { FaPlus } from 'react-icons/fa';
import PermissionListItem from './PermissionListItem';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import { backend } from '../../actions/backend';
import Restricted from '../../PermissionProvider/Restricted';

class PermissionList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userType: this.props.userType,
		}

		this.onAddClick = this.onAddClick.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		if (props.userType.permissions.length !== state.userType.permissions.length) {
			return {
				userType: props.userType
			}
		} else return null
	}

	onAddClick(event) {
		event.stopPropagation();
		event.preventDefault()
		this.props.onAddClick(this.state.userType);
	}

	async onDeleteClick(permissionId) {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/user-types/${this.state.userType.id}/permissions/${permissionId}`,
				method: 'delete'
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Permissão removida com sucesso');
			this.props.reloadList();
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (
			<div className='col-12'>
				<div className='cursor-pointer my-2 p-3 border rounded row' data-bs-toggle='collapse' data-bs-target={`#permissions${this.state.userType.id}`}>
					<div className='col-6'>
						<span style={{ lineHeight: '38px' }}>{this.state.userType.name}</span>
					</div>
					<div className='col-6 text-end'>
						<Restricted to={['access:grant']}><button className='btn float-end' onClick={this.onAddClick}><FaPlus /></button></Restricted>
					</div>
				</div>
				<div id={`permissions${this.state.userType.id}`} className='collapse'>
					<ul className='list-group px-3'>{
						this.state.userType.permissions.map(permission => <PermissionListItem key={permission.id} permission={permission} onDeleteClick={this.onDeleteClick} />)
					}</ul>
				</div>
			</div>
		);
	}
}

export default connect(null, { showModal, backend })(PermissionList);