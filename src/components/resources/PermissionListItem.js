import React, { Component } from 'react';
import { FaTrash } from 'react-icons/fa'
import Restricted from '../../PermissionProvider/Restricted';

class PermissionListItem extends Component {
	constructor(props) {
		super(props);
		this.state = {
			permission: this.props.permission
		}

		this.onClick = this.onClick.bind(this);
	}

	onClick(event) {
		event.preventDefault();
		this.props.onDeleteClick(this.state.permission.resource.id)
	}

	render() {
		return (<div className='row justify-content-between border-bottom'>
			<div className='col-6'>
				<span style={{ lineHeight: '38px' }}>{this.state.permission.resource.name}</span>
			</div>
			<div className='col-6 text-end'>
				<Restricted to={['access:revoke']}><button className='btn' type='button' onClick={this.onClick}><FaTrash /></button></Restricted>
			</div>
		</div>);
	}
}

export default PermissionListItem;