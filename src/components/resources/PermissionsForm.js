import React, { Component } from 'react';
import { showModal } from '../../actions/response-modal';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { Modal } from 'react-bootstrap';
import Select from 'react-select';

class PermissionsForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			show: this.props.show,
			userTypeId: this.props.userTypeId,
			resources: this.props.resources,
			selectedResources: []
		}

		this.close = this.close.bind(this);
		this.onResourcesChange = this.onResourcesChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		if (props.show !== state.show) {
			return {
				show: props.show,
				resources: props.resources,
				userTypeId: props.userTypeId
			}
		}
		return null;
	}

	onResourcesChange(values) {
		this.setState({
			selectedResources: values.map(selectObj => selectObj.value)
		})
	}

	close() {
		this.props.close(false);
	}

	async onSubmit(event) {
		event.preventDefault();
		try {
			const response = this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/user-types/${this.state.userTypeId}/permissions`,
				method: 'post',
				data: {
					resources_ids: this.state.selectedResources
				}
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Permissão salva com sucesso');
			this.props.close(true);
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (<div>
			<Modal animation={false} show={this.state.show} onHide={this.close} centered>
				<form id='address-form' onSubmit={this.onSubmit}>
					<Modal.Header closeButton>
						<Modal.Title>Adicionar permissões</Modal.Title>
					</Modal.Header>
					<Modal.Body className='row'>
						<div className='col-lg-12'>
							<label className='form-label' htmlFor='resource'>Permissões: </label>
							<Select placeholder='Permissões...'
								isMulti={true}
								options={this.state.resources.map(resource => ({ value: resource.id, label: resource.name }))}
								onChange={this.onResourcesChange} required />
						</div>
					</Modal.Body>
					<Modal.Footer>
						<a className='btn' onClick={this.close}>Cancelar</a>
						<button className='btn btn-primary'>Salvar</button>
					</Modal.Footer>
				</form>
			</Modal>
		</div>)
	}
}

export default connect(null, { showModal, backend })(PermissionsForm);