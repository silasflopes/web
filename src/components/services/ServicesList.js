import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Router, Switch, Route } from 'react-router-dom';
import { backend } from '../../actions/backend';
import ServiceListItem from './ServiceListItem';
import MessagesList from '../messages/MessagesList';

class ServicesList extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.state = {
			servicesList: [],
			partnerView: this.props.partnerView,
			activeService: this.props.history.location?.state,
			load: false
		}

		this.onSelected = this.onSelected.bind(this);
		this.reloadList = this.reloadList.bind(this);
		this.setActive = this.setActive.bind(this);
	}

	async componentDidMount() {
		const services = await this.props.getList();
		this.setState({
			servicesList: services
		})
	}

	onSelected(service) {
		this.setActive(service);
		const path = (this.state.partnerView) ? `/partner/services/${service.id}/messages` : `/services/${service.id}/messages`;
		return this.history.push(path);
	}

	async reloadList(service) {
		this.setState({
			activeService: service,
			servicesList: await this.props.getList()
		});
	}

	setActive(service) {
		this.setState({
			activeService: service
		})
	}

	render() {
		const content = this.state.servicesList.length === 0 ? <div>Nenhum serviço cadastrado ainda...</div> :
			<div className='row justify-content-center'>{
				this.state.servicesList.map(service => {
					return (<ServiceListItem isActive={this.state.activeService && this.state.activeService.id === service.id} onSelected={this.onSelected} key={service.id} item={service} isPartnerView={this.state.partnerView} />)
				})
			}</div>
		return (<div className='d-flex h-100'>
			<div id='services-holder' className='d-flex h-100 flex-column'>
				<h3 className='p-3'>Conversas</h3>
				<div className='overflow-y-auto overflow-x-hidden fill-height ps-3'>{content}</div>
			</div>
			<div id='messages-holder' className='container-fluid p-0'>
				{this.state.servicesList.length === 0 ? '' :
					<Router history={this.history}>
						<Switch>
							<Route exact path={['/services/:id([0-9]+)/messages', '/partner/services/:id([0-9]+)/messages']}
								component={(props) => <MessagesList {...props}
									partnerView={this.state.partnerView}
									service={this.state.activeService}
									reloadList={this.reloadList}
									getService={this.props.getOne}
									setActive={this.setActive} />}></Route>
						</Switch>
					</Router>
				}
			</div>
		</div>);
	}
}

export default connect(null, { backend })(ServicesList)