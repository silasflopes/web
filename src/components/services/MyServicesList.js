import React, { Component } from 'react';
import { connect } from 'react-redux';
import { backend } from '../../actions/backend';
import { getUserCookie } from '../../store/cookies';
import ServicesList from './ServicesList';

class MyServicesList extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.getList = this.getList.bind(this);
		this.getOne = this.getOne.bind(this);
	}

	async getList() {
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/services`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			return response.payload.data;
		} catch (error) {
			console.log(error);
		}
	}

	async getOne(id) {
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/services/${id}`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			return response.payload.data;
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (<ServicesList getList={this.getList} getOne={this.getOne} history={this.history} partnerView={false} />);
	}
}

export default connect(null, { backend })(MyServicesList)