import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ReactStars from 'react-rating-stars-component';

class ProfessionalListItem extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;
		this.state = {
			professional: this.props.professional
		}

		this.onItemClick = this.onItemClick.bind(this);
	}

	onItemClick(event) {
		event.preventDefault();
		return this.history.push(`/services/new`, this.state);
	}

	render() {
		const fullname = `${this.state.professional.name} ${this.state.professional.surname}`;
		const services = this.state.professional.offeredServiceTypes.map(offeredService => `${offeredService.serviceType.name}: R$${offeredService.baseValue.replace('.', ',')}`).join(' | ');
		const payments = this.state.professional.acceptedPaymentMethods.map(acceptedPayment => `${acceptedPayment.paymentMethod.name}`).join(' | ');
		return (<div className='col-lg-12 py-2 px-3 mb-2 border rounded cursor-pointer' onClick={this.onItemClick}>
			<div className='row'>
				<div className='col-6'>
					<p className='text-truncate' title={fullname}>{fullname}</p>
					<div title={this.state.professional.rate === 0 ? 'Sem avaliações' : `${this.state.professional.rate} estrela${this.state.professional.rate > 1 ? 's' : ''}`}><ReactStars count={5} size={20} value={this.state.professional.rate} edit={false} /></div>
				</div>
				<div className='col-6'>
					<p className='text-truncate' title={services}>{services}</p>
					<p className='text-truncate' title={payments}>{payments}</p>
				</div>
			</div>
		</div>)
	}
}

export default withRouter(ProfessionalListItem);