import React, { Component } from 'react';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import QueryString from 'query-string';
import { showModal } from '../../actions/response-modal';
import ProfessionalList from './ProfessionalList';

class Index extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		const queryParams = QueryString.parse(this.props.location.search);
		this.state = {
			searchTerm: 'q' in queryParams ? queryParams.q : '',
			professionalList: []
		}

		this.onSearch = this.onSearch.bind(this);
		this.search = this.search.bind(this);
	}

	componentDidMount() {
		this.search(this.state.searchTerm);
	}

	async search(searchTerm) {
		if (searchTerm.length >= 3) {
			try {
				const response = await this.props.backend({
					url: `${process.env.REACT_APP_API_URL}/service-types/users?q=${searchTerm}`,
					method: 'get'
				}, false);
				if ('error' in response) throw response.error;
				this.setState({
					professionalList: response.payload.data
				});
			} catch (error) {
				console.log(error);
			}
		}
	}
	onSearch(event) {
		event.preventDefault();
		this.setState({
			searchTerm: event.target.value
		});
		this.history.push(`/services/search?q=${event.target.value}`)
		this.search(event.target.value);
	}

	render() {
		return (
			<div className='row'>
				<div className='col-12'>
					<input id='search' onChange={this.onSearch} value={this.state.searchTerm} className='form-control' type='search' />
				</div>
				<ProfessionalList list={this.state.professionalList} />
			</div>
		)
	}
}

export default connect(null, { showModal, backend })(Index);