import React, { Component } from 'react';

class ServiceListItem extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isActive: this.props.isActive
		}
		this.onSelected = this.onSelected.bind(this);
	}

	onSelected() {
		this.props.onSelected(this.props.item)
	}

	static getDerivedStateFromProps(props, state) {
		if (state.isActive !== props.isActive) {
			return {
				servicesList: props.isActive
			}
		} else return null
	}

	render() {
		return (
			<div value={this.props.item.id} className={'border-bottom border-lg border-solid cursor-pointer w-100 p-2' + (this.props.isActive ? ' active' : '')} onClick={this.onSelected}>
				<div className='row'>
					<div className='col-5 text-truncate'><strong>{this.props.isPartnerView ? `${this.props.item.user.name} ${this.props.item.user.surname}` : `${this.props.item.professional.name} ${this.props.item.professional.surname}`}</strong></div>
					<div className='col-7 text-truncate'>{this.props.item.serviceType.name}</div>
				</div>
				<div className='text-truncate'></div>
			</div>
		)
	}
}

export default ServiceListItem;