import React, { Component } from 'react';
import { backend } from '../../actions/backend';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import { Modal } from 'react-bootstrap';
import { getUserCookie } from '../../store/cookies';
import AddressListItem from '../addresses/AddressListItem';
import moment from 'moment-timezone'

class ServiceView extends Component {
	constructor(props) {
		super(props);

		this.state = {
			service: this.props.service,
			show: (this.props.show) ? true : false,
			description: this.props.service.description,
			value: this.props.service.value
		}
		this.user = getUserCookie();

		this.onSubmit = this.onSubmit.bind(this);
		this.closeModal = this.closeModal.bind(this)
		this.onDescriptionChange = this.onDescriptionChange.bind(this);
		this.onValueChange = this.onValueChange.bind(this);
		this.onRejectClick = this.onRejectClick.bind(this);
		this.onAcceptClick = this.onAcceptClick.bind(this);
		this.getUrl = this.getUrl.bind(this);
		this.editService = this.editService.bind(this);
	}

	componentDidMount() {
		if (this.state.show || this.state.service) {
			this.setState({
				show: true
			})
		}
	}

	static getDerivedStateFromProps(props, state) {
		if ((props.service && !state.service) || (props.service && state.service && props.service.id !== state.service.id))
			return {
				service: props.service,
				description: props.service.description,
				value: props.service.value,
				show: props.show
			}
		return {
			show: props.show
		}
	}

	async onSubmit(event) {
		event.preventDefault();
		const data = {
			value: parseFloat(this.state.value)
		};
		if (!this.props.partnerView) data.description = this.state.description;
		await this.editService(data);

	}

	async onRejectClick(event) {
		event.preventDefault();
		const data = (!this.props.partnerView) ? { user_acceptance: false } : { professional_acceptance: false };
		await this.editService(data);
	}

	async onAcceptClick(event) {
		event.preventDefault();
		const data = (!this.props.partnerView) ? { user_acceptance: true } : { professional_acceptance: true };
		await this.editService(data);
	}

	getUrl() {
		return this.props.partnerView ? `${process.env.REACT_APP_API_URL}/partners/services/${this.state.service.id}`
			: `${process.env.REACT_APP_API_URL}/users/${this.user.email}/services/${this.state.service.id}`;
	}

	async editService(data) {
		try {
			const url = this.getUrl();
			const request = {
				url,
				method: 'patch',
				data
			}
			const response = await this.props.backend(request);
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Serviço salvo com sucesso');
			this.props.reloadList(response.payload.data);
		} catch (error) {
			console.log(error);
		}
	}
	closeModal() {
		this.props.closeModal();
	}

	onDescriptionChange(event) {
		this.setState({
			description: event.target.value
		})
	}

	onValueChange(event) {
		this.setState({
			value: parseFloat(event.target.value).toFixed(2)
		})
	}

	render() {
		let myAcceptance = '';
		let otherAcceptance = '';
		if (typeof this.state.service.userValueAcceptance === 'number') {
			const acceptance = (this.state.service.userValueAcceptance === 1) ? 'aceitou' : 'recusou';
			if (!this.props.partnerView) myAcceptance = `Você ${acceptance} o valor`;
			else otherAcceptance = `${this.state.service.user.name} ${acceptance} o valor`;

		}
		if (typeof this.state.service.professionalValueAcceptance === 'number') {
			const acceptance = (this.state.service.professionalValueAcceptance === 1) ? 'aceitou' : 'recusou';
			if (this.props.partnerView) myAcceptance = `Você ${acceptance} o valor`;
			else otherAcceptance = `${this.state.service.professional.name} ${acceptance} o valor`;
		}
		const acceptanceButtons = <div><a className='btn text-danger' onClick={[2, 3].includes(this.state.service.status.id) ? null : this.onRejectClick}>Recusar</a>
			<button className='btn btn-success' onClick={this.onAcceptClick} disabled={[2, 3].includes(this.state.service.status.id)}>Aprovar</button></div>
		return (
			<Modal size='lg' animation={false} show={this.state.show} onHide={this.closeModal} centered>
				<form id='service-type-form' className='' onSubmit={this.onSubmit}>
					<Modal.Header closeButton>
						<Modal.Title>Visualizar serviço</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className='text-end'>
							<span className='text-info'><small>Aberto em: {moment(this.state.service.createdAt).format('DD/MM/YYYY HH:mm')}</small></span>
						</div>
						<div className='row mt-2'>
							<div className='col-12 col-lg'>
								<span>Solicitante: {`${this.state.service.user.name}  ${this.state.service.user.surname}`}</span>
							</div>
							<div className='col-12 col-lg'>
								<span>Profissional: {`${this.state.service.professional.name}  ${this.state.service.professional.surname}`}</span>
							</div>
							<div className='col-12 col-lg'>
								<span>Tipo: {this.state.service.serviceType.name}</span>
							</div>
						</div>
						<div className='row mt-2'>
							<div className='col-12 col-lg'>
								<span>Data: {moment(this.state.service.serviceDate).format('DD/MM/YYYY')}</span>
							</div>
							<div className='col-12 col-lg'>
								<span>Pagamento: {this.state.service.paymentMethod.name}</span>
							</div>
						</div>
						<div className='form-group mt-2'>
							<label className='form-label' htmlFor='service-description'>Descrição:</label>
							<textarea disabled={this.state.service.user.email !== this.user.email || myAcceptance.length > 0 || otherAcceptance.length > 0 || [2, 3].includes(this.state.service.status.id)} className='form-control' rows='5' required style={{ resize: 'none' }} type='textarea' id='service-description' value={this.state.description} onChange={this.onDescriptionChange} />
						</div>
						<div className='mt-2'>
							{this.state.service.addresses.length > 0 ? <div><label htmlFor='addresses'>Endereços</label>
								<div id='addresses'>{
									this.state.service.addresses.map(address => <AddressListItem readonly={true} key={address.id} item={{ ...address, address: address.addressName }} />)
								}</div></div> : ''}
						</div>
						<div className='row mt-2'>
							<span>Valor inicial: R${this.state.service.baseValue}</span>
						</div>
						<div className='row form-group mt-2'>
							<label htmlFor='service-value' className='form-label col-6 col-lg-6 col-form-label text-end'>Valor sugerido:</label>
							<div className='col-6 col-lg-3'>
								<input type='number' min='1.00' disabled={myAcceptance.length > 0 || otherAcceptance.length > 0 || [2, 3].includes(this.state.service.status.id)} className='form-control' id='service-value' value={this.state.value} onChange={this.onValueChange} />
							</div>
							<div className='col-12 col-lg-3 text-end mt-lg-0 mt-2'>
								{myAcceptance.length > 0 ? myAcceptance : acceptanceButtons}
							</div>
							<div className='col-12 text-end'>
								<span>{otherAcceptance}</span>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer>
						<a className='btn' onClick={this.closeModal}>Cancelar</a>
						<button className='btn btn-primary' disabled={myAcceptance.length > 0 || otherAcceptance.length > 0 || [2, 3].includes(this.state.service.status.id)}>Salvar</button>
					</Modal.Footer>
				</form>
			</Modal>
		)
	}
}

export default connect(null, { showModal, backend })(ServiceView);