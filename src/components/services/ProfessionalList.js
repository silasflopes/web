import React, { Component } from 'react';
import ProfessionalListItem from './ProfessionalListItem';

class ProfessionalList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			list: 'list' in this.props ? this.props.list : []
		}
	}

	static getDerivedStateFromProps(props) {
		return {
			list: props.list
		}
	}

	render() {
		return (<div className='col-12 my-3'>{
			this.state.list.length === 0 ? 'Nenhum profissional oferece o serviço procurado'
				: this.state.list.map(professional => <ProfessionalListItem key={professional.email} professional={professional} />)
		}</div>);
	}
}

export default ProfessionalList;