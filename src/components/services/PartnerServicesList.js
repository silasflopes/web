import React, { Component } from 'react';
import { connect } from 'react-redux';
import { backend } from '../../actions/backend';
import ServicesList from './ServicesList';

class PartnerServicesList extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.getList = this.getList.bind(this);
		this.getOne = this.getOne.bind(this)
	}

	async getList() {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/partners/services`,
				method: 'get'
			});

			if ('error' in response) throw response.error;

			return response.payload.data;
		} catch (error) {
			console.log(error);
		}
	}

	async getOne(id) {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/partners/services/${id}`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			return response.payload.data;
		} catch (error) {
			console.log(error);
		}
	}
	render() {
		return (<ServicesList getList={this.getList} getOne={this.getOne} history={this.history} partnerView={true} />);
	}
}

export default connect(null, { backend })(PartnerServicesList);