import { backend } from '../../actions/backend';
import React, { Component } from 'react';
import AddressList from '../addresses/AddressList';
import { getUserCookie } from '../../store/cookies';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import Restricted from '../../PermissionProvider/Restricted';
import Forbidden from '../Forbidden';

class ServicesForm extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		if (!this.props.location.state)
			this.history.push('/services/search');

		this.professional = this.props.location.state.professional;

		this.state = {
			professional_email: this.professional.email,
			base_value: 0,
			service_type_id: '',
			description: '',
			payment_method_id: '',
			addresses: [],
			service_date: ''
		}

		this.onServiceTypeChange = this.onServiceTypeChange.bind(this);
		this.onDescriptionChange = this.onDescriptionChange.bind(this);
		this.onPaymentMethodChange = this.onPaymentMethodChange.bind(this);
		this.onAddressesChange = this.onAddressesChange.bind(this);
		this.onServiceDateChange = this.onServiceDateChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.back = this.back.bind(this);
	}
	onServiceTypeChange(event) {
		const id = (event.target.value !== '') ? parseInt(event.target.value) : event.target.value
		const serviceType = this.professional.offeredServiceTypes.find(offeredService => offeredService.serviceType.id === id);
		const baseValue = (serviceType) ? parseInt(serviceType.baseValue) : 0;
		this.setState({
			service_type_id: id,
			base_value: baseValue
		});
	}
	onDescriptionChange(event) {
		this.setState({
			description: event.target.value
		});
	}
	onPaymentMethodChange(event) {
		this.setState({
			payment_method_id: (event.target.value !== '') ? parseInt(event.target.value) : event.target.value
		});
	}
	onAddressesChange(addresses) {
		this.setState({
			addresses
		});
	}
	onServiceDateChange(event) {
		this.setState({
			service_date: event.target.value
		});
	}
	async onSubmit(event) {
		event.preventDefault();
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/services`,
				method: 'post',
				data: this.state
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Orçamento registrado com sucesso');
			return this.history.push(`/services/${response.payload.data.id}`, response.payload.data);
		} catch (error) {
			console.log(error);
		}
	}
	back() {
		this.history.goBack();
	}
	render() {
		return (<Restricted fallback={Forbidden} to={['user:read', 'service:read', 'service:create', 'status:read', 'offered-service:read', 'service-type:read', 'payment-method:read', 'accepted-payment:read']}>
			<div>
				<form onSubmit={this.onSubmit}>
					<div className='mt-3'>
						<span>Solicitando orçamento com <strong>{this.professional.name}</strong></span>
					</div>
					<div className='mt-3'>
						<label className='form-label' htmlFor='service-type'>Tipo de serviço:</label>
						<select required className='form-control' id='service-type' value={this.state.service_type_id} onChange={this.onServiceTypeChange}>
							<option value=''>Selecione um serviço...</option>
							{this.professional.offeredServiceTypes.map(offeredService =>
								<option key={offeredService.serviceType.id} value={offeredService.serviceType.id}>{offeredService.serviceType.name}</option>
							)}
						</select>
					</div>
					<div className='mt-3'>
						<label className='form-label' htmlFor='description'>Descrição:</label>
						<textarea className='form-control' rows='5' required style={{ resize: 'none' }} type='textarea' id='description' value={this.state.description} onChange={this.onDescriptionChange} />
					</div>
					<div className='mt-3'>
						<label className='form-label' htmlFor='payment-method'>Forma de pagamento:</label>
						<select required className='form-control' id='payment-method' value={this.state.payment_method_id} onChange={this.onPaymentMethodChange}>
							<option value=''>Selecione um pagamento...</option>
							{this.professional.acceptedPaymentMethods.map(acceptedPayment =>
								<option key={acceptedPayment.paymentMethod.id} value={acceptedPayment.paymentMethod.id}>{acceptedPayment.paymentMethod.name}</option>
							)}
						</select>
					</div>
					<div className='mt-3'>
						<label className='form-label' htmlFor='service-date'>Data do serviço:</label>
						<input id='service-date' onChange={this.onServiceDateChange} type='date' value={this.state.service_date} required className='form-control' />
					</div>
					<div className='mt-3'>
						<label className='form-label' htmlFor='addresses'>Endereços:</label>
						<AddressList asInput={true} onSelected={this.onAddressesChange} />
					</div>
					<div className='mt-3 text-end'>
						<input type='hidden' id='base-value' value={this.state.base_value} /><span>A partir de: <strong>R${this.state.base_value}</strong></span>
					</div>
					<div className='mt-3 text-end'>
						<a className='btn' onClick={this.back}>Voltar</a>
						<button className='btn btn-primary'>Finalizar</button>
					</div>
				</form>
			</div>
		</Restricted>)
	}
}

export default connect(null, { showModal, backend })(ServicesForm);