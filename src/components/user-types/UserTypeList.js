import React, { Component } from 'react';
import { backend } from '../../actions/backend';
import { showModal } from '../../actions/response-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserCookie } from '../../store/cookies';

class UserTypeList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			user_types: [],
			selected: ''
		}

		this.getTypes = this.getTypes.bind(this);
	}

	async getTypes() {
		try {
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/user-types`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			const user = getUserCookie();
			const userTypes = []
			if (user && user !== 'null' && user.userType.id === 1)
				userTypes.push({ id: 1, name: 'Administrator' });

			userTypes.push(...response.payload.data);
			this.setState({
				user_types: userTypes
			});
		} catch (error) {
			console.log(error);
		}
	}

	async componentDidMount() {
		this.getTypes();
	}

	static getDerivedStateFromProps(props, state) {
		if (props.selected !== state.selected) {
			return {
				selected: props.selected
			}
		}
		return null;
	}

	render() {
		if (this.props.asInput) {
			return (
				<select disabled={this.props.disabled} className='form-control' value={this.state.selected} onChange={this.props.onChange} id={this.props.id} placeholder={this.props.placeholder} required={this.props.required}>
					<option value=''>Selecione um tipo...</option>
					{this.state.user_types.map(type => <option key={type.id} value={type.id}>{type.name}</option>)}
				</select>);
		} else {
			return (
				<ul id={this.props.id}>
					{this.state.user_types.map(type => <li key={type.id}>{type.name}</li>)}
				</ul>);
		}
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ showModal, backend }, dispatch);
}

export default connect(null, mapDispatchToProps)(UserTypeList);