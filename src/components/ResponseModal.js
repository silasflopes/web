import { Modal } from 'react-bootstrap';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showModal } from '../actions/response-modal';

class ResponseModal extends Component {
	constructor(props) {
		super(props);

		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		this.props.showModal('', '');
	}
	render() {
		return (
			<Modal animation={false} show={this.props.show} onClick={this.closeModal} onHide={this.closeModal}>
				<Modal.Header closeButton>
					<Modal.Title>{this.props.title}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<span>{this.props.message}</span>
				</Modal.Body>
			</Modal>
		);
	}
}

function mapStateToProps(state) {
	return { ...state.modal };
}

export default connect(mapStateToProps, { showModal })(ResponseModal);