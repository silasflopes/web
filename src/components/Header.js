import React, { Component } from 'react';
import { FaBars } from 'react-icons/fa'
import logo from '../images/logo.png'

class Header extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (<nav className='header'>
			<header className='navbar px-3'>
				<div className='navbar-brand'><img src={logo} height="34px" /> <strong>HIREME</strong></div>
				<div className='text-end'>
					<button type='button' className='btn navbar-toggler d-md-none menu-toggler' data-bs-toggle='collapse' data-bs-target='#sidebar' aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
						<FaBars className='navbar-toggler-icon' size='30px' />
					</button>
				</div>
			</header>
		</nav>);
	}
}

export default Header