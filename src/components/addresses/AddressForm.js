import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import { Modal } from 'react-bootstrap';
import { getUserCookie } from '../../store/cookies';
import { backend } from '../../actions/backend';

class AddressForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			load: false,
			name: '',
			address: '',
			number: 0,
			complement: '',
			district: '',
			city: '',
			state: '',
			country: '',
			zipcode: '',
			show: ('show' in this.props && typeof this.props.show === 'boolean') ? this.props.show : true,
			id: this.props.id
		}

		this.onNameChange = this.onNameChange.bind(this);
		this.onAddressChange = this.onAddressChange.bind(this);
		this.onNumberChange = this.onNumberChange.bind(this);
		this.onComplementChange = this.onComplementChange.bind(this);
		this.onDistrictChange = this.onDistrictChange.bind(this);
		this.onCityChange = this.onCityChange.bind(this);
		this.onStateChange = this.onStateChange.bind(this);
		this.onCountryChange = this.onCountryChange.bind(this);
		this.onZipcodeChange = this.onZipcodeChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.closeForm = this.closeForm.bind(this);
	}

	componentDidMount() {
		if (this.state.id) {
			this.setState({
				load: true,
				show: true
			})
		}
	}

	componentDidUpdate() {
		if (this.state.load && this.state.id) {
			this.getAddress();
		}
	}

	static getDerivedStateFromProps(props, state) {
		if (parseInt(props.id) !== parseInt(state.id)) {
			return {
				id: props.id,
				show: props.show,
				load: true
			}
		} else if (props.show !== state.show) {
			return {
				show: props.show
			}
		}
		return null;
	}
	async getAddress() {
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/addresses/${this.state.id}`,
				method: 'get'
			})
			if ('error' in response) throw response.error;
			this.setState({ ...response.payload.data, load: false });
		} catch (error) {
			this.setState({
				load: false
			});
		}
	}

	onNameChange(event) {
		this.setState({
			name: event.target.value
		});
	}

	onAddressChange(event) {
		this.setState({
			address: event.target.value
		});
	}

	onNumberChange(event) {
		this.setState({
			number: event.target.value
		});
	}

	onComplementChange(event) {
		this.setState({
			complement: event.target.value
		});
	}

	onDistrictChange(event) {
		this.setState({
			district: event.target.value
		});
	}

	onCityChange(event) {
		this.setState({
			city: event.target.value
		});
	}

	onStateChange(event) {
		this.setState({
			state: event.target.value
		});
	}

	onCountryChange(event) {
		this.setState({
			country: event.target.value
		});
	}

	onZipcodeChange(event) {
		let zipcode = event.target.value.replace(/[^\d]+/g, '');
		if(zipcode.indexOf('-') < 0 && zipcode.length > 5){
			zipcode = `${zipcode.substring(0,5)}-${zipcode.substring(5)}`
		}
		this.setState({
			zipcode
		});
	}

	async onSubmit(event) {
		event.preventDefault();
		try {
			const user = getUserCookie();
			const data = {
				name: this.state.name,
				address: this.state.address,
				number: parseInt(this.state.number),
				complement: this.state.complement,
				district: this.state.district,
				city: this.state.city,
				state: this.state.state,
				country: this.state.country,
				zipcode: this.state.zipcode
			};
			let method;
			let url;
			if (this.state.id) {
				method = 'patch'
				url = `${process.env.REACT_APP_API_URL}/users/${user.email}/addresses/${this.state.id}`;
			} else {
				method = 'post'
				url = `${process.env.REACT_APP_API_URL}/users/${user.email}/addresses`;
			}

			const response = await this.props.backend({
				url,
				method,
				data
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Endereço salvo com sucesso');
			this.props.closeModal(true);
		} catch (error) {
			console.log(error);
		}
	}

	closeForm() {
		this.props.closeModal(false);
	}

	render() {
		return (
			<Modal size='lg' animation={false} show={this.state.show} onHide={this.closeForm} centered>
				<form id='address-form' onSubmit={this.onSubmit}>
					<Modal.Header closeButton>
						<Modal.Title>Adicionar novo endereço</Modal.Title>
					</Modal.Header>
					<Modal.Body className='row'>
						<div className='col-lg-12 mt-3'>
							<label className='form-label' htmlFor='address-name'>Nome do endereço: </label>
							<input id='address-name' className='form-control' placeholder='Nome do endereço' type='text' value={this.state.name} onChange={this.onNameChange} required />
						</div>
						<div className='col-lg-8 mt-3'>
							<label className='form-label' htmlFor='address'>Logradouro: </label>
							<input id='address' className='form-control' placeholder='Logradouro' type='text' value={this.state.address} onChange={this.onAddressChange} required />
						</div>
						<div className='col-lg-4 mt-3'>
							<label className='form-label' htmlFor='address-number'>Número: </label>
							<input id='address-number' className='form-control' placeholder='Número' type='number' min='1' value={this.state.number} onChange={this.onNumberChange} required />
						</div>
						<div className='col-lg-4 mt-3'>
							<label className='form-label' htmlFor='address-complement'>Complemento: </label>
							<input id='address-complement' className='form-control' placeholder='Complemento' type='text' value={this.state.complement} onChange={this.onComplementChange} required />
						</div>
						<div className='col-lg-4 mt-3'>
							<label className='form-label' htmlFor='address-district'>Bairro: </label>
							<input id='address-district' className='form-control' placeholder='Bairro' type='text' value={this.state.district} onChange={this.onDistrictChange} required />
						</div>
						<div className='col-lg-4 mt-3'>
							<label className='form-label' htmlFor='address-city'>Cidade: </label>
							<input id='address-city' className='form-control' placeholder='Cidade' type='text' value={this.state.city} onChange={this.onCityChange} required />
						</div>
						<div className='col-lg-4 mt-3'>
							<label className='form-label' htmlFor='address-state'>Estado: </label>
							<input id='address-state' className='form-control' placeholder='Estado' type='text' value={this.state.state} onChange={this.onStateChange} required />
						</div>
						<div className='col-lg-4 mt-3'>
							<label className='form-label' htmlFor='address-country'>País: </label>
							<input id='address-country' className='form-control' placeholder='País' type='text' value={this.state.country} onChange={this.onCountryChange} required />
						</div>
						<div className='col-lg-4 mt-3'>
							<label className='form-label' htmlFor='address-zipcode'>CEP: </label>
							<input id='address-zipcode' maxLength='9' className='form-control' placeholder='CEP' type='text' value={this.state.zipcode} onChange={this.onZipcodeChange} required />
						</div>
					</Modal.Body>
					<Modal.Footer>
						<a className='btn' onClick={this.closeForm}>Cancelar</a>
						<button className='btn btn-primary'>Salvar</button>
					</Modal.Footer>
				</form>
			</Modal>
		)
	}
}

export default connect(null, { showModal, backend })(AddressForm);