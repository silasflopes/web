import React, { Component } from 'react';
import { FaEdit, FaTrash } from 'react-icons/fa';
import Restricted from '../../PermissionProvider/Restricted';

class AddressListItem extends Component {
	constructor(props) {
		super(props);

		this.state = {
			checked: false
		}

		this.onEditClick = this.onEditClick.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		this.onSelected = this.onSelected.bind(this);
	}

	onEditClick() {
		this.props.onEditClick(this.props.item);
	}

	onDeleteClick() {
		this.props.onDeleteClick(this.props.item);
	}

	onSelected(event) {
		this.setState({
			checked: event.target.checked
		});
		this.props.onSelected(this.props.item)
	}

	render() {
		const editButton = <Restricted to={['user:read', 'address:read', 'address:edit']}><button onClick={this.onEditClick} className='btn'><FaEdit /></button></Restricted>
		const deleteButton = <Restricted to={['user:read', 'address:read', 'address:delete']}><button onClick={this.onDeleteClick} className='btn'><FaTrash /></button></Restricted>
		return (
			<div value={this.props.item.id} className='col-lg-4 my-2'>
				<div className='card'>
					<div className='card-header'>
						<div className='row'>
							<div className='col-6 align-self-center'>
								<strong>{this.props.item.name}</strong>
							</div>
							<div className='col-6 text-end align-self-center'>
								{this.props.readonly ? '' : editButton}
								{this.props.readonly ? '' : deleteButton}
							</div>
						</div>
					</div>
					<div className='card-body'>
						<div className='row'>
							<div className='col-1 align-self-center'>
								{(this.props.asInput) ? <input type='checkbox' className='' value={this.state.checked} id={`address${this.props.item.id}`} onChange={this.onSelected} /> : ''}
							</div>
							<div className='col-10'>
								<p>{this.props.item.address}, {this.props.item.number}, {this.props.item.complement}</p>
								<p>{this.props.item.district}, {this.props.item.city}-{this.props.item.state}, {this.props.item.country}</p>
								<p>{this.props.item.zipcode}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default AddressListItem;