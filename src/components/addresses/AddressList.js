import React, { Component } from 'react';
import { FaPlus } from 'react-icons/fa';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import AddressListItem from './AddressListItem';
import { getUserCookie } from '../../store/cookies';
import ConfirmDialog from '../ConfirmDialog';
import AddressForm from './AddressForm';
import { backend } from '../../actions/backend';
import Restricted from '../../PermissionProvider/Restricted';

class AddressList extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.state = {
			showFormDialog: this.props.match?.params.id ? true : false,
			showDeleteDialog: false,
			editId: this.props.match?.params.id,
			deleteId: undefined,
			deleteText: '',
			addressList: [],
			asInput: this.props.asInput ? this.props.asInput : false,
			selectedList: []
		}

		this.getList = this.getList.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		this.onEditClick = this.onEditClick.bind(this);
		this.onSelected = this.onSelected.bind(this);
		this.closeDeleteDialog = this.closeDeleteDialog.bind(this);
		this.showForm = this.showForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.deleteAddress = this.deleteAddress.bind(this);
	}

	componentDidMount() {
		this.getList();
	}

	async getList() {
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/addresses`,
				method: 'get'
			});
			if ('error' in response) throw response.error;
			this.setState({
				addressList: response.payload.data
			});
		} catch (error) {
			console.log(error)
		}
	}

	async deleteAddress(id) {
		try {
			const user = getUserCookie();
			const response = await this.props.backend({
				url: `${process.env.REACT_APP_API_URL}/users/${user.email}/addresses/${id}`,
				method: 'delete'
			});
			if ('error' in response) throw response.error;
			this.props.showModal('Operação bem sucedida', 'Endereço excluído com sucesso');
			this.getList();
		} catch (error) {
			console.log(error);
		}
	}

	onDeleteClick(item) {
		this.setState({
			showDeleteDialog: true,
			deleteId: item.id,
			deleteText: <span>Tem certeza que deseja exclui o endereço <strong>{item.name}</strong>?</span>
		});
	}

	onEditClick(item) {
		this.setState({
			showFormDialog: true,
			editId: item.id
		});
		if (!this.state.asInput)
			return this.history.push(`/addresses/${item.id}`)
	}

	showForm(event) {
		event.preventDefault();
		this.setState({
			showFormDialog: true,
			editId: null
		});
	}

	closeDeleteDialog() {
		this.setState({
			showDeleteDialog: false,
			deleteId: undefined
		})
	}

	closeForm(reload = false) {
		this.setState({
			showFormDialog: false
		});
		if (this.state.editId && !this.state.asInput)
			this.history.push('/addresses')
		if (reload)
			this.getList();
	}

	onSelected(item) {
		const selected = this.state.selectedList;
		const index = selected.findIndex(selected => selected.id === item.id)
		// Remove se encontrar
		if (index > -1)
			selected.splice(index, 1);
		else
			selected.push(item);

		this.setState({
			selectedList: selected
		});

		this.props.onSelected(selected)
	}

	render() {
		return (<div>
			<div className='text-end'>
				<Restricted to={['user:read', 'address:create', 'address:read']}><button className='btn' onClick={this.showForm}><FaPlus /></button></Restricted>
			</div>
			<div className='row'>{
				this.state.addressList.length > 0 ?
					this.state.addressList.map(address => {
						return (<AddressListItem onSelected={this.onSelected} onEditClick={this.onEditClick} asInput={this.state.asInput} onDeleteClick={this.onDeleteClick} key={address.id} item={address} />)
					}) : <div className='col-12'>Nenhum endereço cadastrado ainda...</div>
			}</div>
			<Restricted to={['user:read', 'address:create', 'address:read']}><AddressForm closeModal={this.closeForm} show={this.state.showFormDialog} id={this.state.editId} /></Restricted>
			<ConfirmDialog show={this.state.showDeleteDialog} id={this.state.deleteId} bodyText={this.state.deleteText}
				closeModal={this.closeDeleteDialog} confirmAction={this.deleteAddress} />
		</div>);
	}
}

export default connect(null, { showModal, backend })(AddressList);