import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showModal } from '../../actions/response-modal';
import ServiceTypeList from '../service-types/ServiceTypeList';
import { FaTimes } from 'react-icons/fa';

class OfferedServiceTypeForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			base_value: 'item' in this.props ? this.props.item.base_value : 0,
			service_type_id: 'item' in this.props ? this.props.item.service_type_id : '',
			isRemoved: false
		}

		this.onBaseValueChange = this.onBaseValueChange.bind(this);
		this.onServiceTypeChange = this.onServiceTypeChange.bind(this);
		this.onRemoveClick = this.onRemoveClick.bind(this);
	}

	static getDerivedStateFromProps(props) {
		if ('item' in props)
			return {
				base_value: props.item.base_value,
				service_type_id: props.item.service_type_id
			}
		else return null;
	}

	onBaseValueChange(event) {
		this.setState({
			base_value: event.target.value
		});
		this.props.onUpdateItem(this.props.index, { base_value: event.target.value, service_type_id: this.state.service_type_id });
	}

	onServiceTypeChange(service_type_id) {
		this.setState({
			service_type_id
		});
		this.props.onUpdateItem(this.props.index, { base_value: this.state.base_value, service_type_id });
	}

	onRemoveClick(event) {
		event.preventDefault();
		this.setState({
			isRemoved: true
		});
		this.props.onRemoveClick(this.props.index);
	}

	render() {
		return (
			<div className='row'>
				<div className='row col-11 col-lg-11 my-2'>
					<div className='col-8'>
						<ServiceTypeList asInput={true} multiple={false} value={this.state.service_type_id} hideAddButton={true} onChange={this.onServiceTypeChange} />
					</div>
					<div className='col-4'>
						<input id={`service-base-value${this.props.index}`} className='form-control' placeholder='Valor base' type='number' min='1' value={this.state.base_value} onChange={this.onBaseValueChange} required />
					</div>
				</div>
				<a className='btn col-1 col-lg-1' onClick={this.onRemoveClick}><FaTimes /></a>
			</div>
		)
	}
}

export default connect(null, { showModal })(OfferedServiceTypeForm);