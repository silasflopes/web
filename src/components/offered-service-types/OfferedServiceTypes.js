import React, { Component } from 'react';
import OfferedServiceTypesForm from './OfferedServiceTypesForm';
import { FaPlus } from 'react-icons/fa';
import Restricted from '../../PermissionProvider/Restricted';

class OfferedServiceTypes extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selected: 'selected' in this.props ? this.props.selected : []
		}
		this.onAddClick = this.onAddClick.bind(this);
		this.onRemoveClick = this.onRemoveClick.bind(this);
		this.onUpdateItem = this.onUpdateItem.bind(this);
	}

	onAddClick(event) {
		event.preventDefault();
		const selected = this.state.selected;
		selected.push({ base_value: '', service_type_id: '' });
		this.setState({
			selected
		})
	}

	onRemoveClick(key) {
		const selected = this.state.selected;
		selected.splice(key, 1)
		this.setState({
			selected
		});
	}

	onUpdateItem(index, item) {
		const selected = this.state.selected;
		selected[index] = item;
		this.setState({
			selected
		});
		this.props.onChange(selected)
	}

	static getDerivedStateFromProps(props, state) {
		if (props.selected !== state.selected) {
			return {
				selected: props.selected
			}
		}
		return null;
	}

	render() {
		return (<div>
			<div className='text-end'>
				<Restricted to={['user:read', 'service-type:read', 'offered-service:create', 'offered-service:delete']}>
					<button className='btn' onClick={this.onAddClick}><FaPlus /></button>
				</Restricted>
			</div>
			<Restricted to={['user:read', 'service-type:read', 'offered-service:create', 'offered-service:delete']}>
				{this.state.selected.map((item, index) => <OfferedServiceTypesForm onUpdateItem={this.onUpdateItem} key={index} index={index} item={item} onRemoveClick={this.onRemoveClick} />)}
			</Restricted>
		</div>);
	}
}

export default OfferedServiceTypes