import { Modal } from 'react-bootstrap';
import React, { Component } from 'react';
import { connect } from 'react-redux'
class ResponseModal extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Modal animation={false} show={this.props.show} dialogClassName='loading-modal' centered>
				<Modal.Body>
					<div className='d-flex justify-content-center'>
						<div className='spinner-border loading text-info'>
							<span className='sr-only'></span>
						</div>
					</div>
				</Modal.Body>
			</Modal>
		);
	}
}

function mapStateToProps(state) {
	return { ...state.loading };
}

export default connect(mapStateToProps, null)(ResponseModal);