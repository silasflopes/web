import React, { Component } from 'react';
import { FaSignOutAlt } from 'react-icons/fa';
import { Link, withRouter } from 'react-router-dom';
import { homePageUrl } from '../constants';
import Restricted from '../PermissionProvider/Restricted';

class Menu extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const path = this.props.location.pathname;
		return (
			<nav className='nav flex-column collapse d-xs-none d-md-flex position-relative' id='sidebar'>
				<ul className='navbar-nav ms-4 mt-4'>
					<Restricted to={['service-type:read', 'user:read', 'review:read', 'accepted-payment:read', 'offered-service:read']}>
						<li className={['nav-item', (path.indexOf('/services/search') > -1 || path.indexOf('/services/new') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to={homePageUrl}>Página inicial</Link></li>
					</Restricted>
					<Restricted to={['service-type:read']}>
						<li className={['nav-item', (path.indexOf('/service-types') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/service-types'>Tipos de Serviços</Link></li>
					</Restricted>
					<Restricted to={['user:read', 'service:read']}>
						<li className={['nav-item', (/^\/services(\/[0-9]*(\/{0,1})(\/messages){0,1}){0,1}$/.test(path)) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/services'>Meus Serviços</Link></li>
					</Restricted>
					<Restricted to={['user:read', 'address:read']}>
						<li className={['nav-item', (path.indexOf('/addresses') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/addresses'>Meus Endereços</Link></li>
					</Restricted>
					<Restricted to={['dashboard']}>
						<li className={['nav-item', (path.indexOf('/dashboards') > -1) ? 'active-menu' : '' ].join(' ')}>
							<Link className='ms-2 nav-link' to='/dashboards'>Dashboards</Link>
						</li>
					</Restricted>
					<Restricted to={['partner']}>
						<li className='nav-item dropdown'>
							<a href='#' data-bs-toggle='collapse' className='ms-2 nav-link' data-bs-target='#partner-menu' role='button' aria-expanded='false'>Área do Parceiro</a>
							<ul className={['nav', 'collapse', 'ps-2', (path.indexOf('/partner/services') > -1) ? 'show' : ''].join(' ')} id='partner-menu'>
								<Restricted to={['partner:services']}>
									<li className={['nav-item', 'w-100', (path.indexOf('/partner/services') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/partner/services'>Orçamentos</Link></li>
								</Restricted>
							</ul>
						</li>
					</Restricted>
					<Restricted to={['admin']}>
						<li className='nav-item dropdown'>
							<a href='#' data-bs-toggle='collapse' className='ms-2 nav-link' data-bs-target='#admin-menu' role='button' aria-expanded='false'>Administração</a>
							<ul className={['nav', 'collapse', 'ps-2', (path.indexOf('/permissions') > -1 || path.indexOf('/resources') > -1 || path.indexOf('/payment-methods') > -1) ? 'show' : ''].join(' ')} id='admin-menu'>
								<Restricted to={['access:read']}>
									<li className={['nav-item', 'w-100', (path.indexOf('/permissions') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/permissions'>Permissões</Link></li>
								</Restricted>
								<Restricted to={['resource:read']}>
									<li className={['nav-item', 'w-100', (path.indexOf('/resources') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/resources'>Recursos do Sistema</Link></li>
								</Restricted>
								<Restricted to={['payment-method:read']}>
									<li className={['nav-item', 'w-100', (path.indexOf('/payment-methods') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/payment-methods'>Formas de pagamento</Link></li>
								</Restricted>
							</ul>
						</li>
					</Restricted>
					<Restricted to={['user:read']}>
						<li className={['nav-item', (path.indexOf('/profile') > -1) ? 'active-menu' : '' ].join(' ')}><Link className='ms-2 nav-link' to='/profile'>Perfil</Link></li>
					</Restricted>
				</ul>
				<div className='navbar bottom-0 position-absolute w-100 border-top border-light'>
					<div className='w-100'>
						<div className='text-end'>
							<Link className='nav-link' to='/logout'><FaSignOutAlt size='20px' /></Link>
						</div>
					</div>
				</div>
			</nav>
		);
	}
}

export default withRouter(Menu);