import React from 'react';

export default (<div className='mx-auto text-center mt-4'>
	<h1>403 - Acesso negado</h1>
	<h5>Você não tem permissões o suficiente para ver essa página.</h5>
</div>);