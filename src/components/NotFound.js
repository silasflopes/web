import React, { Component } from 'react';

class NotFound extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (<div className='mx-auto text-center mt-4'>
			<h1>404 - Oh não!</h1>
			<h5>Parece que a página que você procura não existe!</h5>
		</div>);
	}
}

export default NotFound;