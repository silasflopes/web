import './App.css';
import logo from './images/logo.png'
import React, { Component } from 'react'
import { FaFacebook, FaKey, FaUser } from 'react-icons/fa'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { authenticate, facebookLogin } from './actions';
import { showModal } from './actions/response-modal';
import { loading } from './actions/loading';
import { homePageUrl } from './constants';
import { getTokenCookie, getUserCookie, setTokenCookie, setUserCookie } from './store/cookies';
import Axios from 'axios';
class Login extends Component {
	constructor(props) {
		super(props);
		this.history = this.props.history;

		this.state = {
			email: '',
			password: '',
			user: null
		}

		this.onEmailChange = this.onEmailChange.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.getProfile = this.getProfile.bind(this);
		this.facebookLogin = this.facebookLogin.bind(this);
		this.onFacebookLogin = this.onFacebookLogin.bind(this);
	}

	onEmailChange(event) {
		this.setState({
			email: event.target.value
		});
	}

	onPasswordChange(event) {
		this.setState({
			password: event.target.value
		});
	}

	async onSubmit(event) {
		event.preventDefault();
		try {
			await this.props.authenticate(this.state.email, this.state.password);
			return this.history.push(homePageUrl);
		} catch (error) {
			const message = ('response' in error && error.response && 'data' in error.response) ? error.response.data.message : 'Algo deu errado com sua requisição. Tente mais novamente tarde';
			this.props.showModal('Ops... Algo deu errado!', (error.code === 401) ? 'Usuário ou senha inválidos' : message);
		}
	}

	componentDidMount() {
		if (this.props.user && this.props.user !== 'null')
			return this.history.push(homePageUrl);
	}
	
	async getProfile(accessToken, userId) {
		this.props.loading(true);
		try {
			const { data } = await Axios.get(`${process.env.REACT_APP_FACEBOOK_GRAPH_URL}/${userId}?fields=first_name,last_name,email&access_token=${accessToken}`)
	
			return data;
		} catch (error) {
			const message = 'Algo deu errado com sua requisição. Tente mais novamente tarde';
	
				this.props.showModal('Ops... Algo deu errado!', message)
			return { error };
		} finally {
			this.props.loading(false);
		}
	}
	
	async facebookLogin(){
		try{
			const { authResponse } = await new Promise(window.FB.login);
			if(authResponse){
				const profile = await this.getProfile(authResponse.accessToken, authResponse.userID);
				await this.props.facebookLogin(profile.id, profile.first_name, profile.last_name);
				return this.history.push(homePageUrl);
			}
		}catch(error){
			console.log(error);
			const message = 'Algo deu errado com sua requisição. Tente mais novamente tarde';
			this.props.showModal('Ops... Algo deu errado!', message)
		}
	}

	async onFacebookLogin(event){
		event.preventDefault();
		await this.facebookLogin()
		return this.history.push(homePageUrl);
	}
	render() {
		return (<div className='mt-4 container'>
			<div className='text-center'>
				<img src={logo} id='logo-img' height='250px' width='250px' alt='Logo' />
			</div>
			<div className='row justify-content-center'>
				<div className="col-lg-6 col-sm-12">
					<form className='text-center' onSubmit={this.onSubmit}>
						<div className='input-group my-3'>
							<span className='input-group-text'><FaUser /></span>
							<input id='email' onChange={this.onEmailChange} className='form-control' value={this.state.email} placeholder='E-mail de usuário' type='email' required />
						</div>
						<div className='input-group my-3'>
							<span className='input-group-text'><FaKey /></span>
							<input id='password' onChange={this.onPasswordChange} className='form-control' value={this.state.password} placeholder='Senha' type='password' required />
						</div>
						<div className='my-1'>
							<button className='btn btn-primary'>Entrar</button>
						</div>
						<div className='my-1'>
							<button className="btn btn-facebook" type='button' onClick={ this.onFacebookLogin }>
								<FaFacebook style={{ marginTop: '-4px' }} /><span>&nbsp;&nbsp;Entrar com Facebook</span>
							</button>
						</div>
						<div className='my-3 text-start'>
							Ainda não possui acesso?<Link className='text-decoration-none' to='/sign-up' > Crie uma conta</Link>
						</div>
						<div>
						</div>
					</form>
				</div>
			</div>
		</div>);
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({ authenticate, showModal, loading, facebookLogin }, dispatch)
}

function mapStateToProps(state) {
	let user;
	let token;
	if (state.user) {
		setUserCookie(state.user.user);
		setTokenCookie(state.user.token);
		user = state.user.user;
		token = state.user.token;
	} else {
		user = getUserCookie();
		token = getTokenCookie();
	}
	return { user, token };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
