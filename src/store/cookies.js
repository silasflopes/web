import Cookies from "universal-cookie";

export function getUserCookie(){
    const cookie = new Cookies();
    return cookie.get('user', { path: '/' });
  }
  
  export function getTokenCookie(){
    const cookie = new Cookies();
    return cookie.get('token', { path: '/' });
  }
  
  export function setUserCookie(user){
    const cookie = new Cookies();
    cookie.set('user', user, { path: '/' });
  }
  
  export function setTokenCookie(token){
    const cookie = new Cookies();
    cookie.set('token', token, { path: '/' });
  }