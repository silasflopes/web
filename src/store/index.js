import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from 'redux-promise';
import Reducers from '../reducers'
import thunkMiddleware from 'redux-thunk'

const createStoreWithMiddleware = applyMiddleware(ReduxPromise, thunkMiddleware)(createStore);
export const Store = createStoreWithMiddleware(
	Reducers,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)