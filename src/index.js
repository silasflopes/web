import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { Router, Switch, Route } from 'react-router-dom';
import { Store } from './store'
import { history } from './_helpers/history';
// Components
import Login from './Login';
import CreateAccount from './components/users/UserForm';
import App from './App';
import ResponseModal from './components/ResponseModal';
import Loading from './components/Loading';
import { CookiesProvider } from 'react-cookie';
import { initFacebookSdk } from './facebook/Facebook';


initFacebookSdk().then(ReactDOM.render(
	<React.StrictMode>
		<CookiesProvider>
			<Provider store={Store}>
				<Router history={history}>
					<Switch>
						<Route exact path='/' component={Login}></Route>
						<Route path='/sign-up' component={CreateAccount}></Route>
						<Route component={App}></Route>
					</Switch>
				</Router>
				<Loading />
				<ResponseModal />
			</Provider>
		</CookiesProvider>
	</React.StrictMode>,
	document.getElementById('root')
));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
