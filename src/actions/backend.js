
import Axios from 'axios';
import { Store } from '../store';
import { getTokenCookie } from '../store/cookies';
import { showModal } from './response-modal';
import { logout } from '.';
import { loading } from './loading';
export const BACKEND_REQUEST = 'BACKEND_REQUEST';

export async function backend(requests, showLoading = true) {
	if (showLoading)
		Store.dispatch(loading(true));
	try {
		const token = getTokenCookie();
		const promises = (Array.isArray(requests))
			? requests.map(request => Axios({ ...request, headers: { ...request.headers, 'Authorization': `Bearer ${token}` } }))
			: [Axios({ ...requests, headers: { ...requests.headers, 'Authorization': `Bearer ${token}` } })];

		const response = await Promise.all(promises);
		return {
			type: BACKEND_REQUEST,
			payload: (Array.isArray(requests)) ? response : response[0]
		}
	} catch (error) {
		let message;
		if ('response' in error && error.response && 'data' in error.response) {
			message = error.response.data.message;
			if (error.response.status === 401) {
				Store.dispatch(logout())
				return { error }
			}
		} else
			message = 'Algo deu errado com sua requisição. Tente mais novamente tarde'

		Store.dispatch(showModal('Ops... Algo deu errado!', message))
		return { type: BACKEND_REQUEST, error };
	} finally {
		if (showLoading)
			Store.dispatch(loading(false));
	}
}