export const RESPONSE_MODAL = 'RESPONSE_MODAL'
export function showModal(title, message) {
	return {
		type: RESPONSE_MODAL,
		payload: {
			title,
			message
		}
	}
}