import Axios from 'axios';
import { setUserCookie } from '../store/cookies';
import { loading } from './loading';
import { Store } from '../store';
export const AUTHENTICATION = 'AUTHENTICATION';
export const FACEBOOK_LOGIN = 'FACEBOOK_LOGIN';
export const LOGOUT = 'LOGOUT';
export async function authenticate(email, password) {
	Store.dispatch(loading(true));
	try {
		const response = await Axios.get(`${process.env.REACT_APP_API_URL}/users/authenticate`, {
			auth: {
				username: email,
				password
			}
		});

		return {
			type: AUTHENTICATION,
			payload: response
		}
	} finally {
		Store.dispatch(loading(false));
	}
}
export function logout() {
	setUserCookie(null);
	return {
		type: LOGOUT,
		payload: null
	}
}
export async function facebookLogin(userId, firstName, lastName){
	Store.dispatch(loading(true));
	try{
		const response = await Axios.post(`${process.env.REACT_APP_API_URL}/users/facebook-login`, { user_id: userId, first_name: firstName, last_name: lastName });

		return {
			type: FACEBOOK_LOGIN,
			payload: response
		}
	}finally{
		Store.dispatch(loading(false));
	}
}