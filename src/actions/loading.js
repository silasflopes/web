export const LOADING = 'LOADING'
export function loading(show) {
	return {
		type: LOADING,
		payload: {
			show
		}
	}
}