import { RESPONSE_MODAL } from '../actions/response-modal';

export default function (state = { message: '', title: '', show: false }, action) {
	switch (action.type) {
		case RESPONSE_MODAL:
			return {
				message: action.payload.message,
				title: action.payload.title,
				show: action.payload.message.length > 0
			};
		default:
			return state;
	}
}