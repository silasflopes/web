import { LOADING } from '../actions/loading';

export default function (state = { show: false }, action) {
	switch (action.type) {
		case LOADING:
			return {
				show: action.payload.show
			};
		default:
			return state;
	}
}