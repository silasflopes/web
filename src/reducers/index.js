import { combineReducers } from "redux";
import LoginReducer from './login';
import ModalReducer from './response-modal';
import LoadingReducer from './loading';

const rootReducer = combineReducers({
	user: LoginReducer,
	modal: ModalReducer,
	loading: LoadingReducer
})

export default rootReducer;