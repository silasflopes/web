import { AUTHENTICATION, FACEBOOK_LOGIN, LOGOUT } from '../actions/index';

export default function (state = null, action) {
	switch (action.type) {
		case AUTHENTICATION:
			return action.payload.data;
		case FACEBOOK_LOGIN:
			return action.payload.data;
		case LOGOUT:
			return null;
		default:
			return state;
	}
}