import { BACKEND_REQUEST } from '../actions/backend';

export default function (state = null, action) {
	switch (action.type) {
		case BACKEND_REQUEST:
			return action.payload;
		default:
			return state;
	}
}